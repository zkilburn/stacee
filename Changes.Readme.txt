This Branch is utilizing the autonomous button on the control box, the gyro, and macro commands Easy Transfer to tell STACEE to enter currentState 2, do a while loop while gyroAngle doesnt equal 90 degrees, and turn to match the gyro angle to 90 degrees. 

Turning is current occuring at a proportional rate to the error in the angle (she turns quickly then slows down as she gets close.

Two variables store the macro state. When macro_command (control box) and macroCommand (comm board) are not equal to 0 then a macro is to be done. The macro_command tells the comm board to change macroCommand to the same value. Some delays had to be implemented to make sure they didnt just oscillate between =0 and =1.. more on that if you ask... But when macro commands =1 they will break out when the macro is completed, or when the button is pressed again. This allows for abort out of macro from the box, and reinitialization of the macro from the box when the current macro is completed.

Note:: STACEE's motors are getting hot and unhappy, the turn rate doesnt stay consistent after the motors get hot.. The drivetrain may be screwing up the calibration constants I am providing. Need to investigate her drivetrain and ensure we are not damaging things.



12-30-13 MK IV

Implemented another macro command to distinguish between different command... localized the while loop to the turn function and allowed for a second command (macroCommand=2) that turns in the opposite direction of macroCommand=1.

Implementation will require the motor controller to not ramp speeds, need direct control with the autonomous system.. 

Also currently STACEE may be accidently being left in the autonomous mode after the system completes... this needs to be fixed



1/1/13 MK V

Pending the destruction of the gears in STACEE due to very tight joystick operation, we are refocusing on developing the screen for the control box.

The code has been overhauled in a number of ways
-cheaterboard RX communication
****buttons are handled in a case switch block
****provides easy upgradability with base structure (needs tested)

-Control box Easy Transfer for cheater board in/out

-variable passing from robot to control board for screen

-macro capabilities of the screen through cheater->control->robot

-macro upgraded to accept 3 macros w/ sub commands

	---
	****macro_command 1 is turning
	------sub_command specifies an angle to turn -180 to 180

	****macro_command 2 is drive
	------sub_command specifies a duration (currently time based)
		should be using encoders

	****macro_command 3 is actuator
	------sub_command specifies a duration (currently time based)
		should be using feedback from actuator

--A lot has been changed so until the we program the screen and cheater board and control and comm board, it is unsure how much will cause issues if any of the new code.. will check back later with revisions as necessary.




1-6-14 MK VI
Worked on the screen communication. Got comms through consistently.. now its not working consistantly.. going to check on the checksum and see if it is working correctly.. appears there is some breakdown in the 4d communication protocol.. 

macros are running but appear to be looping.. need to see where the command is persisting and get rid of it..



1-7-14 MK VII

Found some errors in the checksum calculation.. fixed and screen is working with some slight moodyness (requires a 4 second delay before hitting another button) relating to the notTooFast variable which may require further tuning.. the screen code has been moved to the control board for simplicity in organization and removal of another potential pitfall.. 

.. the robot appears to continue the macro commands indefinitely.. need some debugging out as well as some TLC..



1-7-14 updated

Screen is functioning while connected to the control box.. the commands are passed almost flawlessly.. 

the macros are now only running once and operation appears very smooth with macro completion (added an aknowledgment for completed macro)
t 
overshoot on turning has been minimized partially, turning still needs tuned but this should be done after the track connectors are replaced to ensure proper operation during tuning. otherwise calibration is bound to change...

Recharging batteries before departure



1-7-14 MK VIII

began a new branch from the trunk.. 

--dealing with the issue of performing a macro (manual) vs. the robot entering autonomous(continued macros)
-- need to declare the usage of the autonomous/macro button.. macros come from screen while autonomous triggered by button??



1-10-14
--Latency seems to be back, not sure which code is getting hung up or why it is occuring. 
--Screen code is freezing on the control board.. sometimes will stop updating outputs but continue accepting and processing inputs

--Added the actuator feedback and control systems. STACEE can do things with her actuator efficiently..

--The drive command was altered to accept time forward/backward using millis() system for efficiency.
** slight hiccup occurs with drive commands coming to rest.. need to look into this

--Stabilized the control of comm board systems to only accept and store valid expected arguments. (needs to be done to control board)

--code must in top performance for the first lego demo this weekend. may need to revise back to a version where things work with no latency.. (store this code in the functional section of the code store)

--Beginning MK XI

1-7-14 Mk XI 

This new version is going to focus specifically on the screen stability, and finalizing all of the commands that will be used in autonomous mode. 

Reducing the joystick latency after macros run is vital, as well as consistent screen output, even if it should be a workaround and not perfect, smoothness is a must.




1-19-14 Mk XII

Wrapping up the MK 12 version of the system. the code has been optimized for communications to provide reliability. the screen is still having issues maintaining a communication with the control board... the buttons will actually continue working but there appears to be an issue with the control board sending updates to the screen.. a reboot of the control board will result in the problem temporarily disappearing.. there are checks on the control board to verify that it is not unnecessarily sending information to the screen when the input data has not changed..

this could be the area that is not updating, but I feel as though it is not the issue.. going to alter and change this code and hopefully discover the issue with through doing this..

moving onto MK 13 the intention is to provide a reliable screen communication and to further the screen development heading toward the autonomous system..

IDEAS for screen:
-want to prototype and develop the screen to have more intuitive control of actuator and autonomous commands
*-make an actuator page(done ish)
-make a drive with differential page
-make a drive distances page
*-investigate the use of sliders with digits to set a range of numbers on the screen as apposed to a thousand buttons.

IDEAS for autonomous
-get a pattern repeat movement and incorporate actuator movements to allow for a detailed show of the gyro/macro functions
-measure the driving by time movement at different commanded values. get a chart of data for prelim auto functions




1-19-14 Mk 13
Accomplished smooth screen updating, no issues so far with any instability or freezing.. I put in a mandatory refresh of all screen items every 2.5 seconds, as well as changing the check for the amount of ttime passed to an absolute value of the difference. guarentee this was the issue.. it was checking if one was greater than the other. but when the counter rolled over it was not catching this case.. 

integrated the actuator screen data and updated the screen. actuator page works to my liking, although someone I'm sure is going to want to change this up :) (igor)...

feedback to angular meter proved sucessful..

next big step is the drive with encoder..
else its IRs

going to develop screen code to have much more functionality
interesting that the counter for millis appears to only be tracking from 0-65535 and no further. 




1-23-14 mk 14

Happy with progress on autonomous and screen to document a beta version naming mk 14 as a development version.. needs tested (: always a good sign to have more code ready than what I will know what to do with. 

plenty of addons coming fresh from this release including a heading calculation from the gyro (persisting throughout the macro turns)(it is a must have for auto navigation and will give an idea of how reasonable the gyro remains after multiple commands..

autonomous is ready to test pending a subtle drive test that will verify the lengths are not orders of magnitude off.. (previous value (duration*0.125) now its (duration *0.025) for resolution to slider control

a lot of statment catching is going on between methods and trickle effect involved so these need to be tested (specifically the macros in autonomous mode)

screen communication continues to get thicker, now would be when I would expect to see latency issues with overloaded data comms handling data. 

added feedback for overshoot error calculations and reports, this has been added to the already existing turning page to hopefully add some numerical approximation of how much overshoot is occuring.

Again as I stated this will be archived as mk 14 and debugged as soon as gears get installed :( 


1-26-14 mk 14

Did some major debugging today after the bootup resulted in all macros freezing and not executing due to the check for continueMacro resulting in a zero when data was not sent as such(at least i do not believe it was being sent a one) assuming the easy transfer return from recieveData() was true when data was not updated or stable..

implementation changed to using a separate variable passed called macro_stop. this variable is normally a zero until the stop command is active. so the erroneous check/find zero results in a continued command..

NOTE!!!!!
it should be noted that when easy transfer doesnt recieve a packet, it will return values of 0 (pending further investigation)

gyro scope tuning tonight resulted in an error margin of <25 degrees.. which is okay but not great.. looking forward to the IRverifyPerpendicular() function ability.. which reminds that we need to start the dev of IR search submethods based on approx sensor locations.

|					|
|					|
|					|
|					|
|	Start Area			|
|					|
|					|
|					|
|					|					
|_____	   _____________________________|
|  0.6m			2.54m		|
|					|					
|	|0.25m				|
|	|				|
________________________________________

These are rough alignments with the lengths that we will see..
if situation is reversed

|					|
|					|
|					|
|					|
|	Start Area			|
|					|
|					|
|					|
|					|					
|___________________________      ______|
| 	   2.54m	           0.6m	|
|					|					
|				|0.25m	|
|				|	|
________________________________________

If another orientation things become complicated

|					|
|					|
|				|	|
|				|	|
|	Start Area		| ? m	|
|				|	|
|				|	|
|				|	|
|					|					
|___________________________      ______|
| 	   2.54m	           0.6m	|
|					|					
|					|
|					|
________________________________________

STACEE should have IRs on all sides capable of doing at least 1.5 meters.. another large limitation is the low range sensing capabilities of each side..

A though for initial localization, continue turning 90 degrees until the low range sensors are in range, this direction is the wall by the target dump area.

WARNING, if stacee turns and moves out of low range (4cm-30cm) she will loose the ability to search for this direction as reference.


IR layout 		|
			|
			|
			|1-5.5m
			|
		    |	|   |0.2-1.5
		    |	    |
		    |	    |
		________________
		|		|
		|		|
	________|		|________
    0.1m-0.8m	|		| 0.1m-0.8m
		|		|	   _________________________
______		|		|		1m  - 5.5m
1m-5.5m		|		|
		|___0.04-0.30___|
		     |	    |  
		     |	|   | 
		     |  |   | 
		        |
			|    
		   0.1-0.8m (4inches-30inch)
			
			|
			|
			|
			|1m-5.5m
			|3ft -18ft
			|
			|
			|

Thinking this might work well as a layout design, 4 sensors on rear, 2 per side  3 in the front
do have a blind spot on three sides at 80 cm to 100cm

0.04 - 0.30  meters
1.57 - 11.81 inches


0.1  - 0.8   meters
4    - 30    inches


0.2  - 1.5   meters
7.87 - 59.05 inches


1    - 5.5   meters
3    - 18    feet



Assuming this setup, we can easily detect both sides of the arena to detect perpendicular/
also we have capability of seeing the start side (0.25m) from any of our sides.. need to create a (recognizeCleanSignal(IR#);)
so that ignorant values can be ignored wisely...

this needs to be tested as soon as physically possible

IR's will need to be modulated from on to off with appropriate wait periods.. every IR reading must be done while it is the only IR on on that side of the robot..
with configuration above this can be done in 4 readings with concurrent math done in each read cycle, all longs/ all 10cm-80cm, left stereo, right stereo

a smarter robot will know which IRs to expect. therefor can sample only certain IRs in a loop.. this needs to be designed and implemented

as far as init, would like to fire all sensors and make logic to determine the direction facing and set heading accordingly..

enough for now

going to attempt to test gyroscope repeatability sometime soon, linking turns back to back in a macro loop

archiving mk 14 beginning mk 15 turning PRO

2-3-14

Merged the autonomous and macro systems .h file.. seems to be logically pleeasing to me, macro system called updateAutonomous(), while there is nowhere else this method will be called

created a few methods to contain initializing the macro system and finishing macro systems.. 

changed the order of the methods in a few files, thinking it reads more logically,, needs a lot more work to spin it around the correct way by using method declaration at the top of the page instead of relying on order.

2-4-14 mk 16

Screen work-- made screen only update the information that is present on the page being displayed... able to catch when each page is initialized, (changed from one to another) and can keep track of which page is being observed. Instead of uselessly updating background variables, the page that is being displayed is the only on to receive updates.

Screen layout has changed-- made a new home page/ menu scheme changes all around..

More code formality implementation to correct readability issues. #define variables with naems that help understandning

2-5-14 mk 16

--fixed many buggy systems and have reliable systems operation..
added case 4 to macrorequest acceptable filter on input to allow d drive configuration.

--gyro scope heading working
--diff / drive working
--got reliable screen operation (through page monitoring for smarter updating)
--passing circles showed need to clear the gyro after each turn was completed.
--system operation very steady and cool..
--archiving with screen code for this version for version control of stable show version

2-11-14 mk 17

--added the communications CRC checking system integrated into the easytransfer library. 
--functionality has been proven but errors in system operation persist
--mk 18 will investigate these issues

2-15-14 mk 18

--IR sensors are installed and am using a duemillanove to process half of them as the rest of the analog read lines are not broken out on the comm board.. this is why there is another ino file included in this version. analog pins 0-4 are being read in and transmitted to the comm board

--Igor and I worked on the filtering technique that is being used and came up with a very gratifying way to filter them (regardless of the interference from each other they are currently experiencing).

--The interference shows the need to be able to control these power lines to the IR's so this is a must on research in order to figure out a way to control these at this moment in time on STACEE before the hardware to do so is built.

--Reading out IR's to the screen resulted in NO change in communications latency and therefor we can assume the fix that was implemented (EasyTransferCRC) has put a stitch or two into the issues and I can assume the communications bus can be opened up even wider than we are using it currently.

--LED control has changed slightly including the startup and comm loss.
------The robot has a flicker at start (no comms)
-------same flicker when comms drop and at signal aquisition.

--stacee no longer sends a single packet until it recieves a good one from the control board.

--further development on IR's is necessary to determine which IR is the valid reading per side (orientation algorithm)

-- also, IR's need to be extensively tested as one of the MID range front sensors that we ordered (20-150cm) was DOA ( or at least I think it was dead when it got here :)  ) so I was forced to go to an old sensor of the same range, which is currently solving that issue

--the long range IR on the front is going to need a capacitor on the VCC to GND to stabilize it further.

--looking forward to giving stacee the brains to use her eyes



2-25-14 mk 18 

--IR readings per side (simplified to a single value per side) has been roughly implemented, and in no way am i satisfied with the quick and dirty organization of this system... it will eventually need to incorporate a detailed sensor analysis, referencing encoders, IRs and gyroscope as a PLANT and generating a value that will represent all of the data within the system. I.e if you are not moving, large changes in the IRs are not necessary to believe, encoders will (probably) be the most reliable sensor system. 

-- the screen code has been massively updated to accommadate the upgrades that will be occuring withing the IR sensor development, and much of this layout is very one directional towards the ideas that I would like to implement and have already begun, 

		-- an IR follow method that uses both the front sensor array and one of the sides, maintaining a close prox to side until front gets in a desired range

		--an drive until IR distance is reached (front and back) 

		--a verify perpendicular (turn 1 degree at a time and search for a value that is less than a stored value. if so choose this new location as perpendicular, and continue the search, once you determine a location that has gained distance you know you have overshot, return back and maybe even check another degree or two past this point to ensure you have located the local minimum.

		-- a validate IR values by basically performing the verify perpendicular test but looking for abnormal changes in sensor readings indicating that the sensor is actually reading an invalid part of the curve (the repeating low range values that normally occur under sensor ranging)

more updates on what is to come in the future probably later today..

trying to stay on top of the updates.

3-2-14

--IR reading calibrations began recently development continues.

--Macros and selection algorithm tuning

--going to implement PID control for most systems soon (development on the library for PID) 

3-2-14
--Generic IR data from datasheet equation

--Communications on screen fix

--Massive updates to sensor reading and macro systems

3-3-14

The current sensor is very roughly wired, and is currently reporting the amperage flowing at a point in time, accuracy to the implied 10th of an amp. Appears to be reporting reliably. The voltage also is routed through and appears to give a roughly accurate measurement, although both of these should be tuned more specifically.

Macro IR's is drawing to a finale, as far as the foundations of the structure are concerned. systems are now working with PID control as opposed to a strictly proportional system, and things do like this alot! 

All PID systems are going to need revisions for the sand as well as this environment, It would be nice if the systems would tune themselves, and I would like to get this done within the week.

The screen control has also jumped ahead a little bit, as the system is now being told to switch screens during a macro and it will automatically switch out of this screen when the macro is completed. This idea should transfer into a much smarter feedback system allowing much more user feedback and smoothness to the user.

Would like to implement a boot screen as well as danger, low voltage warnings, not to mention over amperage, and a multitude of available sensor system updates. 

Overall the system development is doing an amazing job and I am going to package this Mk 18 into a finalized IR development archive!! 

Moving onward and upward towards AUTONOMOUS development.

NOTE: do need to do our Encoder system development but for now I am thinking that we will just move towards the autonomous systems and worry about doing encoder integration in time :/

Goodbye mk 18

3-4-14

Creating the PID monitor to allow a viewing of the PID system on the robot at any time.

The system will need to be able to select which PID system is reporting to the control box/screen at any time and it would be nice to specifically understand which one is reporting back as well as a button to toggle between the PID systems we configure for it to read out. Making 4 digits displaying (error/integral/derivative/output) as well as a STRINGS entity (learning another 4d entity) to display which PID system is showing allowing for easy navigation without a lookup table of numbers  -- overall its a great addition for competition tuning as well as tuning now (i basically need it to see what is going on to tune kp,ki,kd values)

--next on the screen neeeds to be the freaking scope entity because it seems to be amazingly flexible and complicated :)

change robot side screen send timer 300->100

implemented changes to the PID library system in order to display features to the screen easily and store a number referring to the data within the PID system (sideError/frontError/turnError) which packages well to get data where it needs to without too many calls. the PID is handed to the updateScreen method which was duplicated to allow for a lack of PID system to not prevent it from being called. should probably be passing a lot of my variables by reference now that I think about it. 

I think that the way i am handling the strings should work but we will see when Im finished programming all of these uProcessors! this is why i am ranting right now. gonna smoke a cig :)

3-5-14

Inserted the ping sensor alternate system for reading the read long IR value (psuedo reading)

// this will allow the system development to continue as if there was a sensor system in place there.

going to take the control box home tonight and work on screen code on my more apt computer than my laptop.. development confirmed of the operation of send constants, going to dev that to the point of PID systems tuning.

3-11-14

IR INIT AUTO and IR MACROS AUTO in AUTO screen,
macro confirm