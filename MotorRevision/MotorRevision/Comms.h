//comms.h

EasyTransfer communicationBoardIn;

unsigned long past_comm_safety_time = 0;

void initializeCommunication()
{
  Serial.begin(57600);
  communicationBoardIn.begin(details(motor), &Serial);
  
}

inline void commSafety()
{
  unsigned long time = millis();
  if (abs(time - past_comm_safety_time) > 1000)
  {
    leftMotorSpeed = 255;
    rightMotorSpeed = 255;
    actuatorSpeed = 255;
  }
}

inline void updateComms()
{
  if(communicationBoardIn.receiveData())
  {
    past_comm_safety_time = millis();
    leftMotorSpeed = motor.leftMotorSpeed;
    rightMotorSpeed = motor.rightMotorSpeed;
    actuatorSpeed = motor.actuatorSpeed;   
  }
  else
  {
    commSafety();
  }
}


