//servos.h

#ifndef SERVOS_H
#define SERVOS_H

#include <Servo.h>

// attage names to servos
static Servo leftMotor;
static Servo rightMotor;
static Servo actuator;

static int currentLeftMotorSpeed = 0;
static int currentRightMotorSpeed = 0;
static int currentActuatorSpeed = 0;

// atteches pins to the servo commands
void initializeServos()
{
  leftMotor.attach(5);
  rightMotor.attach(6);
  actuator.attach(3);
}

inline static void updateRamp()
{
  static unsigned long currentMotorLeft = 0;
  static unsigned long previousMotorLeft = 0;
  static unsigned long currentMotorRight = 0;
  static unsigned long previousMotorRight = 0;
  static unsigned long currentActuator = 0;
  static unsigned long previousActuator = 0;
  
  
  
  currentMotorLeft = millis();
  currentMotorRight = millis();
  currentActuator = millis();
  
  
  
  if((currentMotorLeft - previousMotorLeft) > time[(currentLeftMotorSpeed + 90)])
  {
    previousMotorLeft = currentMotorLeft;
    if(currentLeftMotorSpeed < leftMotorSpeed)
    {
      currentLeftMotorSpeed += 1;
    }
    else if(currentLeftMotorSpeed > leftMotorSpeed)
    {
      currentLeftMotorSpeed -= 1;
    }
    else
    {
      currentLeftMotorSpeed = leftMotorSpeed;
    }

  }

  if((currentMotorRight - previousMotorRight) > time[(currentRightMotorSpeed + 90)])
  {
    previousMotorRight = currentMotorRight;

    if(currentRightMotorSpeed < rightMotorSpeed)
    {
      currentRightMotorSpeed += 1;
    }
    else if(currentRightMotorSpeed > rightMotorSpeed)
    {
      currentRightMotorSpeed -= 1;
    }
    else
    {
      currentRightMotorSpeed = rightMotorSpeed;
    }
  }

  if(currentActuator - previousActuator > 5)
  {
    previousActuator = currentActuator;

    if(currentActuatorSpeed < actuatorSpeed)
    {
      currentActuatorSpeed += 1;
    }
    else if(currentActuatorSpeed > actuatorSpeed)
    {
      currentActuatorSpeed -= 1;
    }
    else
    {
      currentActuatorSpeed = actuatorSpeed;
    }
  }

  if(actuatorSpeed == 255 || leftMotorSpeed == 255 || rightMotorSpeed == 255)
  {
    currentLeftMotorSpeed = 0;
    currentRightMotorSpeed = 0;
    currentActuatorSpeed = 0;
  }
  }
  

inline static void updateSpeeds()
{
  leftMotor.write((currentLeftMotorSpeed + 90));
  rightMotor.write((currentRightMotorSpeed + 90));
  actuator.write((currentActuatorSpeed + 90));
}

inline void updateServos()
{
  updateRamp();
  updateSpeeds();
}

#endif

