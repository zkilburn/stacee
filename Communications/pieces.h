//
//-----------------------------Removal of the tuning system for the time being----------------------------------------------
//initial setup
float magnitudeCO = 0.4;
float errorMargin = 0;
static int firstWay = 0;
static int nowWay = 0;
bool tunedUP = 0;
bool finishedTuning = 0;
//If there is a request to perform a macro


//macro init
//var for recording the presence of an overshoot
bool overShot = false;
//Determine which way we will start turning
if (macroAngle > angleSet)
firstWay = RIGHT;
else
  firstWay = LEFT;






//while loop turning
//if you have changed turning directions and still tuning
if (firstWay != nowWay && !finishedTuning)
{
  //if you have been tuning up before now
  if (tunedUP)
  {
    //record margin of overshoot error
    errorMargin = abs(macroAngle - angleSet);
    //return to last tuning setting and
    //magnitudeCO*= 0.85;
    //finish tuning
    finishedTuning = true;
  }
  else
  {
    //record error overshoot
    errorMargin = abs(macroAngle - angleSet);
    //reduce the turning speed
    //magnitudeCO *= 0.9;
    //record which direction we are turning now
    firstWay = nowWay;
    //record that we had overshoot
    overShot = true;
  }
}







// decision making
if (angleSet > macroAngle)
{
  nowWay = LEFT;                                         //record current turning direction for tuning
  magnitude = (angleSet - macroAngle) * magnitudeCO;     //set magnitude to difference between angle * coeff
  magnitude = gyroCheck(magnitude);                      //verify that the gyro says we are turning (not too fast or too slow)
  magnitude = constrain(magnitude, motorLow, motorHigh); //make sure magnitude is not an angry man
  turnLeft(magnitude);                                   //turn to the left with rectified magnitude
}
else if (angleSet < macroAngle)
{
  nowWay = RIGHT;                                        //record current turning direction for tuning
  magnitude = (macroAngle - angleSet) * magnitudeCO;     //set magnitude to difference between angle * coeff
  magnitude = gyroCheck(magnitude);                      //verify that the gyro says we are turning (not too fast or too slow)
  magnitude = constrain(magnitude, motorLow, motorHigh); //make sure magnitude is not an angry man
  turnRight(magnitude);                                  //turn to the right with rectified magnitude
}




//make decisions about tuning after while loop
if (!overShot && !finishedTuning)
{
  //make a note that we have tuned up
  tunedUP = 1;
  //change coefficient of turning magnitude
  //magnitudeCO *= 1.1;
}


//---------------------------------------------------------------------------




//helpful debugging counting system with two separate counting triggers on variable intervals
bool stuff = true;
int handy = 0, int plyable = 0;
while (stuff)
{
  handy += 1;
  if (handy > 3) //interval 1
  {
    //do something or use interval counter for delaying per run of repeating section of code
    plyable += 1;
    handy = 0;
  }
  if (plyable > 50) //interval 2
  {
    //do something or use interval counter for delaying per run of repeating section of code
    plyable = 0;
  }
}

// The attempted tuning through derivative
inline bool followRight(int rangeSide, int rangeFront)
{
  float tstart = micros();
  float now = tstart;
  float dt = 0;
  static float prevSide = rightS;
  static float dSide = 0;
  continueMacro = 0;
  //while the actuator is not at the target(within 1)
  while ((frontS >= rangeFront) && continueMacro == 0)
  {
    now = micros();
    dt = (now - tstart) / 1000000;
    tstart = now;
    //update the value of the actuator
    updateIRSensors();
    //choose how fast to command
    int magnitude = (frontS - rangeFront);
    dSide = ((prevSide - rightS) / dt) * 1000000; //pos on approach/ neg on depart
    prevSide = rightS;
    if (frontS < 100)
    {
      magnitude -= 5;
    }
    magnitude = constrain(magnitude, motorLow, (motorHigh / 2));

    if (dSide > 0) // towards wall -- turn left
    {
      differentialDrive(magnitude, dSide); // positive command
    }
    else if (dSide < 0)
    {
      differentialDrive(magnitude, dSide); //negative command
    }
    else
    {
      variableDrive(magnitude);
    }
    //report to the screen
    updateScreen();

    //check for command halt
    if (from_control.receiveData())
    {
      continueMacro = fromControl.macro_stop;
    }
    //send motor data
    to_motor.sendData();
  }
  //end command
  allStop();
  motor_unStick();

  //return if halt
  return (continueMacro == 0);
}
//D=142857.1429(1/mv)+200



      //This structure checks each pass and will not continue
      //if one of these gets cancelled midstream (it will cancel all)

      //Orient
      //      if(followMap(start,forward))
      //      {
      //        //Traverse
      //        if(followMap(digSite,forward))
      //        {
      //          //Mine
      //          actuatorMine();
      //
      //          //Backtrack
      //          if(followMap(start,backward))
      //          {
      //            //Dump
      //            //approachDump();
      //            actuatorDump();
      //          }
      //        }
      //      }






    bool continuable = true;
      do
      {
        continuable = followLeft(leftS, 120);
        betweenMacro();
        if (continuable == true)
        {
          continuable = turn(-90);
          betweenMacro();
        }
      }
      while (continuable);


bool continuable = true;
      do
      {
        continuable = turn(90);
        betweenMacro();
        if (continuable == true)
        {
          continuable = followLeft(200);
          betweenMacro();
        }
        if (continuable == true)
        {
          continuable = turn(-90);
          betweenMacro();
        }
        if (continuable == true)
        {
          continuable = followLeft(200);
          betweenMacro();
        }
        if (continuable == true)
        {
          continuable = actuatorMine();
          betweenMacro();
        }
        if (continuable == true)
        {
          continuable = driveUntilIRRear(120);
          betweenMacro();
        }
        if (continuable == true)
        {
          continuable = actuatorDump();
          betweenMacro();
        }        
      }
      while (continuable);



