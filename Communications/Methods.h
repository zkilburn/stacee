

/******************************************************************************/
/*                           Method Declarations                              */
/******************************************************************************/

/*********************************************************/
/*             Sensors.h Method Declarations             */
/*********************************************************/
//Helper methods
int validRead(int signal,  int average);            //only believe and store a %value of sensor change
bool varIsAbout(int var, int val, int plusMinus);   //compare a variable to a value and check if its within rangel

//given a raw sensor value do a calculation to determine distance in cm
float calcLongIR(float rawValue, int sensor);
float calcMidIR(float rawValue);
float calcFrontStereo(float rawValue, int sensor);
float calcBackStereo(float rawValue);

//Given all of the sensor inputs per side, select which value is valid and use it as side reading
int selectFront(int LongF, int midL, int midR, int prev);
int selectSide(int LongS, int midS, int prev);
int selectBack(int LongB, int midB, int shortL, int shortR, int prev);

bool midFCheck(int MF);   //check 20-150
bool longCheck(int L);    //check 75-550
bool midCheck(int M);     //check 10-80
bool shortCheck(int S);   //check 2-30

int avgF(int L, int M1, int M2);        //Average the front values to aquire an initial value
int avgB(int L, int M, int S1, int S2); //Average the back values to aquire an initial value
int avgS(int L, int M);                 //Average the side values to aquire an initial value
inline void initIR();
void IRinputInit();
void IRoutputPowerInit();

static inline void actuatorUpdate();
void updateIRSensors();

/*********************************************************/
/*             Autonomous Method Declarations             */
/*********************************************************/
//Autonomous features methods
void updateAutonomous();
inline bool straightShot(bool continuable);
bool fixPositionX(int currentX, int targetX, int direction);
bool fixPositionY(int currentY, int targetY, int direction);
bool makeHeading(int globalAngle);
bool followMap(int location, int direction);
/*********************************************************/
/*               Macro Method Declarations               */
/*********************************************************/
//Macro System Features Methods
inline void initMacroSystem(int state);
inline void finishMacro();
void betweenMacro();
inline void allStop();
inline void motor_unStick();
inline bool turn(int setAngle);
inline bool drive(int length);
inline bool actuator(int target);
inline bool actuatorDump();
inline bool actuatorMine();
inline bool dDrive(int offset);
inline void updateRobotTurn();
inline void variableDrive(int mag);
inline void differentialDrive(int mag, int offset);
inline void actuatorAdjust(int mag);
inline void IRstreaming();
inline bool followBoth(int distanceF);
inline bool followRight(int rangeSide, int rangeFront);
inline bool followLeft(int rangeSide, int rangeFront);
inline bool driveUntilIRFront (int distance);
inline bool driveUntilIRRear (int distance);
inline void updateScreen(PID runningPID);
inline void updateScreen();
inline void scootch();
inline void levelLR();
inline void turnHelp(int mag);
inline void updateAutonomousFeatures(int cmd);
inline bool backupAfterRock(int distance);
inline bool backupAfterMineMove(int distance);
inline bool doStartTurnAndCenter(int initTurn, bool R_L);
inline bool straightShot2(bool continuable);
inline int IRDetermineLocation(int frontS, int backS, int leftS, int rightS);
inline bool pushRock(int distance);
inline bool actuatorMine2();


/*********************************************************/
/*              Gyro Method Declarations               */
/*********************************************************/

inline int sampleGyroZ();
int MPU6050_read(int starts, uint8_t *buffer, int sized);

