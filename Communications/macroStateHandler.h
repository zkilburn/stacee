



//---METHOD TO START A MACRO SYSTEM COMMAND------
inline void initMacroSystem(int state) {
  setColor(1570, 0, 1600);
  betweenMacro();
  delay(5);
  //Tell the control box we have entered the macro system (send to control box)
  //will get resent as data gets updated with method screen update
  toControl.macroCommand = macroRequest;
  to_control.sendData();
  delay(5);
  //Show Everyone We Are In Autonomous Mode (On board)
  currentState = state;
  updateLeds();

  //Motor Preparation (Send all stop to motor board)
  allStop();
  motor_unStick();

  //Sensor Preparation
  //Set gyro to 0' before we use it at all
  zeroInternalAngle();
  actuatorUpdate();
}

//--------METHOD TO FINISH A MACRO SYSTEM COMMAND-------------
inline void finishMacro()
{
  setColor(1570, 0, 1600);
  //Tell the control box that macro complete (send to control)
  toControl.macroCommand = 0;
  toControl.macro_complete = 1;
  delay(10);
  to_control.sendData();
  sender = 0;
  while (macroRequest != 0)  {
    if (from_control.receiveData())
      macroRequest = fromControl.macro_command;

    sender += 1;
    delay(50);
    if (sender >= 10)
    {
      to_control.sendData();
      sender = 0;
    }
  }
  //Sensor Refresh/Clear
  zeroInternalAngle();
  angleSet = 0;
  firstShot = true;
  delay(25);
  commTimer.resetTimer();
  //Return the Robot to drive mode
  currentState = 1;
  macroRequest = 0;
  toControl.macro_complete = 0;
}

//************************************MACRO SYSTEM LOOP FOR UPDATES************************************
//-----------Loop that checks for macro information and runs them appropriately-----------------------
inline void updateMacro() {
  if (macroRequest != 0)  {
    initMacroSystem(MACRO);
    //Check to see which macro was requested (macro_command)
    //use the macro_sub_command for turn angle and options

    sub_command = fromControl.macro_sub_command;
    //MACRO REQUESTS (NOT SETTINGS OR AUTO)
    switch (macroRequest) {
      //------Gyroscope macros---------
    case TURN:
      turn(sub_command);
      break;
      //------future home of encoder macros--------
    case DRIVE:
      drive(sub_command);
      break;
      //---------actuator macros-------
    case ACTUATOR:
      switch (sub_command) {
      case 250:
        actuatorDump();
        break;
      case 255:
        actuatorMine();
        break;
      case 145:
        actuatorMine();
        break;
      default:
        if (sub_command < 101 && sub_command > -1)
          actuator(sub_command);
        break;
      }
      break;
      //---------Drive while turning------------
    case 4:
      dDrive(sub_command);
      break;
      //--------------------IR Systems Macros----------------------
    case 5:
      driveUntilIRFront(100);
      break;
    case 6:
      driveUntilIRRear(30);
      break;
    case 7:
      followBoth(100);
      break;
    case 8:
      followLeft(leftS, 100);
      break;
    case 9:
      if (midFCheck(IRSFL) && midFCheck(IRSFR))
        levelLR();
      break;
    case 10:
      //Turn towards distance side (1= short 2=long)
      if (sub_command == 1)
      {
        //turn towards short
      }
      else if (sub_command == 2)
      {
        //turn towards long
      }
      break;
    case 11:
      //- Validate IR Data (scootch)
      break;

    case 68: //ADDITIONAL FEATURES
      updateAutonomousFeatures(sub_command);
      break;
    case 69:  //Autonomous Systems
      //pass command off to the autonomous handler
      autoSet = 1;
      //for now changing with a "handy" variable
      autoState = sub_command;  //sub_command
      updateAutonomous();
      //handy+=1;
      //if (handy>3) handy=0;
      break;
      //Variable manipulation systems;
    case 70:
      motorHigh = sub_command;
      break;
    case 71:
      motorLow = sub_command;
      break;
    case 72:
      PIDTimer.setInterval(sub_command);
      break;
    case 73:
      sideKpL = sub_command;
      break;
    case 74:
      sideKiL = sub_command;
      break;
    case 75:
      sideKdL = sub_command;
      break;
    case 76:
      gyroCorrectL = sub_command;
      break;
    case 77:
      sideDeadL = sub_command;
      break;
    case 78:
      gyroKp = sub_command;
      break;
    case 79:
      gyroKi = sub_command;
      break;
    case 80:
      gyroKd = sub_command;
      break;
    case 81:
      sideKpR = sub_command;
      break;
    case 82:
      sideKiR = sub_command;
      break;
    case 83:
      sideKdR = sub_command;
      break;
    case 84:
      gyroCorrectR = sub_command;
      break;
    case 85:
      sideDeadR = sub_command;
      break;
    case 86:
      slowingDistance = sub_command;
      break;
    }
    finishMacro();
  }
}
inline void updateAutonomousFeatures(int cmd) {
  switch (cmd) {
  case 1:     //Gyro RESET
    headingZero();
    break;
  case 2:    //reInit (IR/Gyro)
    frontS = 999,
    backS = 999,
    leftS = 999,
    rightS = 999;
    calibrateZero(200);
    break;
  case 3: //unstick IR
    frontS = 999,
    backS = 999,
    leftS = 999,
    rightS = 999;
    break;
  }
}




void updateAutonomous(int location)
{
  switch (location) {
  case L1:
    continuable = doStartTurnAndCenter(0, Lside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case L2:
    continuable = doStartTurnAndCenter(60, Lside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case L3:
    continuable = doStartTurnAndCenter(120, Lside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case L4:
    continuable = doStartTurnAndCenter(180, Lside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case L5:
    continuable = doStartTurnAndCenter(-120, Lside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case L6:
    continuable = doStartTurnAndCenter(-60, Lside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case R1:
    continuable = doStartTurnAndCenter(-180, Rside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case R2:
    continuable = doStartTurnAndCenter(-120, Rside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case R3:
    continuable = doStartTurnAndCenter(-60, Rside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case R4:
    continuable = doStartTurnAndCenter(0, Rside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case R5:
    continuable = doStartTurnAndCenter(60, Rside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case R6:
    continuable = doStartTurnAndCenter(120, Rside);
    while (continuable)
      continuable = straightShot(continuable);
    break;
  case 13:
    continuable = true;
    do
    {
      continuable = straightShot2(continuable);
    }
    while (continuable);
    break;
    //      continuable = true;
    //      do
    //      {
    //        continuable = followLeft(leftS, 120);
    //        betweenMacro();
    //        if (continuable == true)
    //        {
    //          continuable = turn(-90);
    //          betweenMacro();
    //        }
    //      }
    //      while (continuable);
    //      break;
  case 14:
    continuable = true;
    do
    {
      continuable = straightShot2(continuable);
    }
    while (continuable);
    break;
  case 15:
    continuable = true;
    do
    {
      continuable = straightShot2(continuable);
    }
    while (continuable);
    break;
  case 16:
    continuable = true;
    do
    {
      continuable = straightShot2(continuable);
    }
    while (continuable);
    break;
  case 17:
    continuable = true;
    do
    {
      continuable = straightShot2(continuable);
    }
    while (continuable);
    break;

  }
}
//************************************AUTONOMOUS HANDLER FOR COMMANDS************************************
void updateAutonomous()
{
  if (autoSet == 1)
  {
    //Assume DEFCON 3 (Nuclear Tactical Avoidance Systems)
    initMacroSystem(AUTO);

    //Standard Autonomous mode to drive course regularly
    //Template in pieces.h
    switch (autoState)
    {
    case 0:
      updateAutonomous(IRDetermineLocation(frontS, backS, leftS, rightS));
      break;

    default:
      updateAutonomous(autoState);
      break;
    }
    autoSet = 0;
    autoState = 0;
  }
}

//-----METHOD TO SEND INFORMATION TO CONTROL BOX------
//Display update during the macro/autonomous commands
inline void updateScreen(PID runningPID)
{
  screenTimer.updateTimer();
  if (screenTimer.timerDone())
  {
    //Actuator gyro
    toControl.bucketAngle = actuatorAngle;
    toControl.gyroAngle = macroAngle;
    //long IR
    //    toControl.IRRightLong = IRLR;
    //    toControl.IRLeftLong = IRLL;
    //    toControl.IRBackLong = IRLB;
    //    toControl.IRFrontLong = IRLF;
    //    //MID IRs
    //    toControl.IRBackMid = IRMB;
    //    toControl.IRLeftMid = IRML;
    //    toControl.IRRightMid = IRMR;
    //    //BACK IRs
    //    toControl.IRBackL = IRSBL;
    //    toControl.IRBackR = IRSBR;
    //    //FRONT IRs
    //    toControl.IRFrontR = IRSFR;
    //    toControl.IRFrontL = IRSFL;
    //simplified IR
    toControl.F = frontS;
    toControl.L = leftS;
    toControl.R = rightS;
    toControl.B = backS;
    //motor speeds
    toControl.motorL = outMotor.leftMotorSpeed;
    toControl.motorR = outMotor.rightMotorSpeed;
    toControl.PIDerror = runningPID.readError();
    toControl.PIDintegral = runningPID.readIntegral();
    toControl.PIDderivative = runningPID.readDerivative() * 10000;
    toControl.PIDoutput = runningPID.readOutput();
    toControl.PIDnumber = runningPID.readNumber();

    to_control.sendData();
  }
}
inline void updateScreen()
{
  screenTimer.updateTimer();
  if (screenTimer.timerDone())
  {
    //Actuator gyro
    toControl.bucketAngle = actuatorAngle;
    toControl.gyroAngle = macroAngle;
    //long IR
    //    toControl.IRRightLong = IRLR;
    //    toControl.IRLeftLong = IRLL;
    //    toControl.IRBackLong = IRLB;
    //    toControl.IRFrontLong = IRLF;
    //    //MID IRs
    //    toControl.IRBackMid = IRMB;
    //    toControl.IRLeftMid = IRML;
    //    toControl.IRRightMid = IRMR;
    //    //BACK IRs
    //    toControl.IRBackL = IRSBL;
    //    toControl.IRBackR = IRSBR;
    //    //FRONT IRs
    //    toControl.IRFrontR = IRSFR;
    //    toControl.IRFrontL = IRSFL;
    //simplified IR
    toControl.F = frontS;
    toControl.L = leftS;
    toControl.R = rightS;
    toControl.B = backS;
    //motor speeds
    toControl.motorL = outMotor.leftMotorSpeed;
    toControl.motorR = outMotor.rightMotorSpeed;

    to_control.sendData();
  }
}

