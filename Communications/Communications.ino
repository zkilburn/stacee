
//communications.ino

//Version 19
//created by zac kilburn and igor vinograd
//2/15/14

// initialize the needed libraries
#include <EasyTransfer.h>
#include <EasyTransferCRC.h>
#include <digitalWriteFast.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <Average.h>
#include <PID.h>
#include <Timers.h>
// include the .h files
#include "Definitions.h"
#include "Variables.h"
#include "Structs.h"
#include "Methods.h"
#include "Leds.h"
#include "Gyroscope.h"
#include "Sensors.h"
#include "Debug.h"
#include "macroStateHandler.h"
#include "MacroTestScripts.h"
#include "Macros.h"

#include "Comms.h"

void setup()
{
 
  initializeLeds();
  initializeSensors();
  initializeMacros();
  initializeGyro(MPU6050_I2C_ADDRESS);
  initializeCommunications();
  //initializeDebug();
}

void loop()
{
  updateGyro();
  updateSensors();
  updateComms();
  updateLeds(); 
  //updateDebug();
}


