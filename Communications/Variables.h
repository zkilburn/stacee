//gyroscope tracking for calibrations
int numberRuns=0;
bool firstShot=true;


bool readyToSend=false;
//-----------------------Library based Timers-------------------------
Timers sensorTimer(4);
Timers commTimer(1000);
Timers screenTimer(76);
//PID systems timer
Timers PIDTimer(25);
//Communications adjustment
Timers latency(8);
Timers sendTime(500);
//arduino helper sensorBaord update rate
Timers checkPacket(5);

//CONTROL systems IR
int slowingDistance=250;
int actuatorDrivingAngle=40;
float sideKpL = 0.025, sideKdL = 40000000, sideKiL = 0; //- negative Kp   500000000 0.06
int gyroCorrectL = 55, sideDeadL = 5;                                  //100000000

float sideKpR = 0.025, sideKdR = 40000000,  sideKiR = 0;//500000000
int gyroCorrectR = 55, sideDeadR = 5;

//PID system GYRO
float gyroKp= 0.5, gyroKi=0, gyroKd=55;


int handy = 0;
bool continuable;
//Error State
int debug;
int errorState = 0;
float frontS = 100, backS = 100, leftS = 100, rightS = 100;


//Maximums for turning power
//#define motorHigh 18 //with sand 35 // WITHOUT SAND 18
//#define motorLow 8 //with sand 25  // WITHOUT SAND 10
int gyroLow = 15;
int gyroHigh = 40;
int motorLow = 8; //8;
int motorHigh = 18; //18;


//------------------------EasyTransfer vars---------------------------
EasyTransferCRC from_control;
EasyTransfer to_motor;
EasyTransferCRC to_control;
EasyTransferCRC sensorBoard;
EasyTransfer fromPower;

//------------------------Autonomous vars---------------------------
int autoSet = 0;
int autoState = 0;

//------------------------Sensors vars---------------------------
#define SIZE 20
//array for data storage
int d0[SIZE];
int d1[SIZE];
int d2[SIZE];
int d3[SIZE];
int d4[SIZE];
int d5[SIZE];
//calculation variables
int stddev0, avg0;
int stddev1, avg1;
int stddev2, avg2;
int stddev3, avg3;
int stddev4, avg4;
int stddev5, avg5;
//data array pointer
int index = 0;
//Data points from sensor board
float D0, D1, D2, D3, D4, D5;

//------------------------Gyroscope  vars---------------------------
int8_t swap;
int macroAngle;
static float zFilt;
static int16_t offset, high, low, z_dead_g;
static float zRaw, zDeg, zRot;
static float dt;
static float compare;
static int16_t samples[500];
static float lastTime;
static float now;
static int error;
float persistentAngle;
int latestFilteredSample = 0;

//------------------------LED  vars---------------------------
int prevColor = 0;
static Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

//------------------------STATE  vars---------------------------
//this is the state stacee is in
// 0 = no comms
// 1 = manual drive
// 2 = autonomous
// the inicial state assumes no comms
int currentState = 0;

//------------------------Screen Data vars---------------------------
//Actuator storage
int actuatorAngle = 0;
//output for heading
int robotHeading = 0;
int raw0;
int raw1;
int raw2;
int raw3;
int raw4;
int raw5;
int raw6;
int raw7;
int raw8;
int raw9;
int raw10;
//------------------------IR vars---------------------------
float IRLR = 0, IRLL = 0, IRLB = 0, IRLF = 0;
float IRMB = 0, IRML = 0, IRMR = 0;
float IRSBL = 0, IRSBR = 0;
float IRSFL = 0, IRSFR = 0;

//------------------------Macro vars---------------------------
int filteredSample;
float errorMargin = 0;

int macroRequest = 0;
//once a macro begins this variable tracks
//continuing the macro..
int continueMacro = 0;
//gyroscope set angle (used for turning)
int angleSet;
int notTooFast = 0;

//------------------------AUTO vars---------------------------
int sub_command = 0;
//x,y location of robot now
int currentLocationX, currentLocationY;
//heading as calculated from gyroscope integration + global reference
int currentHeading;
//array to encompas the traversal area and map it for available
int obstacleMapping[6][1][1];

//------------------------COMM vars---------------------------
int sender = 0;
//---------------------------Power Vars----------------------
int volts, amps, watts;
