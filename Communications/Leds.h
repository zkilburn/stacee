//LEDs.h

void setColor(int color);
void flicker75();

void initializeLeds()
{
  pwm.begin();
  pwm.setPWMFreq(1600);
  pinModeFast(13, OUTPUT);
}

static inline void updateLeds()
{
  if (currentState == 0)
  {
    setColor(RED);
  }
  else if (currentState == 1)
  {
    setColor(BLUE);
  }
  else if (currentState == 2 || currentState == 3)
  {
    setColor(GREEN);
  }

  static unsigned long previous = 0;
  static unsigned long current = 0;
  static int state = HIGH;
  current = millis();
  if (abs(current - previous) > 1000)
  {
    previous = current;
    state = !state;
    digitalWriteFast(13, state);
  }
}

void setColor(int color)
{
  if (color == TEAL)
  {
    pwm.setPWM(1, 0, 1600);
    pwm.setPWM(4, 0, 1600);
    pwm.setPWM(2, 0, 0);
    pwm.setPWM(5, 0, 0);
    pwm.setPWM(0, 0, 900);
    pwm.setPWM(3, 0, 900);
  }
  if (color == RED)
  {
    pwm.setPWM(0, 0, 0);
    pwm.setPWM(1, 0, 0);
    pwm.setPWM(3, 0, 0);
    pwm.setPWM(4, 0, 0);
    pwm.setPWM(2, 0, 1600);
    pwm.setPWM(5, 0, 1600);
  }
  else if (color == BLUE)
  {
    pwm.setPWM(1, 0, 0);
    pwm.setPWM(2, 0, 0);
    pwm.setPWM(4, 0, 0);
    pwm.setPWM(5, 0, 0);
    pwm.setPWM(0, 0, 1600);
    pwm.setPWM(3, 0, 1600);
  }
  else if (color == GREEN)
  {
    pwm.setPWM(0, 0, 0);
    pwm.setPWM(2, 0, 0);
    pwm.setPWM(3, 0, 0);
    pwm.setPWM(5, 0, 0);
    pwm.setPWM(1, 0, 1600);
    pwm.setPWM(4, 0, 1600);
  }
  if (color == YELLOW)
  {
    pwm.setPWM(1, 0, 1600);
    pwm.setPWM(4, 0, 1600);
    pwm.setPWM(2, 0, 1600);
    pwm.setPWM(5, 0, 1600);
    pwm.setPWM(0, 0, 0);
    pwm.setPWM(3, 0, 0);
  }
}
void setColor(int R, int G, int B){
   pwm.setPWM(0,0,B);
   pwm.setPWM(1,0,G);
   pwm.setPWM(2,0,R);
   pwm.setPWM(3,0,B);
   pwm.setPWM(4,0,G);
   pwm.setPWM(5,0,R);
}

void flicker75()
{
  delay(75);
  setColor(GREEN);
  delay(75);
  setColor(BLUE);
  delay(75);
  setColor(TEAL);
}

void packetWait()
{
  if (prevColor == 1)
  {
    setColor(RED);
    prevColor = 0;
  }
  else
  {
    setColor(GREEN);
    prevColor = 1;
  }
}


