

//structs.h

//Gyro Struc
typedef union gyro_union
{
  struct
  {
    uint8_t z_gyro_h;
    uint8_t z_gyro_l;
  }
  reg;
  struct
  {
    int16_t z_gyro;
  }
  value;
};
gyro_union gyro;
//------------------------COMM structs---------------------------

// data from control to robot
struct fromContoltoComm
{
  int8_t leftMotorSpeed;
  int8_t rightMotorSpeed;
  int8_t actuator;
  uint8_t state;
  uint8_t macro_command;
  int macro_sub_command;
  uint8_t macro_stop;
}
fromControl;


// data out to motor
struct fromCommtoMotor
{
  int leftMotorSpeed;
  int rightMotorSpeed;
  int actuator;
}
outMotor;

struct fromCommtoControl
{
  uint8_t macroCommand;
  int gyroAngle;
  int robotHeading;
  uint8_t bucketAngle;
  bool macro_complete;
//  uint8_t errorMargin;
//  int IRFrontLong;
//  int IRBackLong;
//  int IRLeftLong;
//  int IRRightLong;
//  uint8_t IRRightMid;
//  uint8_t IRFrontL;
//  uint8_t IRFrontR;
//  uint8_t IRLeftMid;
//  uint8_t IRBackMid;
//  uint8_t IRBackL;
//  uint8_t IRBackR;
  //int IRFrontLongR;
  //int IRBackLongR;
  //int IRLeftLongR;
  //int IRRightLongR;
  //uint8_t IRRightMidR;
  //uint8_t IRFrontLR;
  //uint8_t IRFrontRR;
  //uint8_t IRLeftMidR;
  //uint8_t IRBackMidR;
  //uint8_t IRBackLR;
  //uint8_t IRBackRR;
//  int debug1;
//  int debug2;
//  int debug3;
//  int debug4;
  int volts;
  int amps;
  int watts;
  int F;
  int L;
  int R;
  int B;
  int8_t motorL;
  int8_t motorR;
  int PIDerror;
  int PIDintegral;
  int PIDderivative;
  int PIDoutput;
  int PIDnumber;
}
toControl;

struct fromSensortoComm
{
  int data0;
  int data1;
  int data2;
  int data3;
  int data4;
  int data5;
} 
fromSensor;

//static fromSensortoComm fromSensor;

struct powerData
{
  long amphours;
  long watthours;
  long averageVolts;
}
power;



