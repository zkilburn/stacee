//Sensors.h
/**************************************************************************************/
/*                            Sensor Systems Init Method                              */
/**************************************************************************************/
inline void initIR(){
  //Populate the data sets (5ms for readings)
  for (int i = 0; i < SIZE; i++)
  {
    d0[i] = analogRead(IRLONGRIGHT);
    d1[i] = analogRead(IRLONGLEFT);
    d2[i] = analogRead(IRLONGFRONT);
    d3[i] = analogRead(IRLONGBACK);
    d4[i] = analogRead(IRFRONTLEFT);
    d5[i] = analogRead(IRFRONTRIGHT);
    delay(5);
  }
  //set sensor selection values to aknowledge they need initialized
  frontS = 999, backS = 999, leftS = 999, rightS = 999;
  //prepare the timer for the sensor
  sensorTimer.resetTimer();
}
void initializeSensors(){
  IRinputInit();
  IRoutputPowerInit();
  pinMode(ACTUATOR, INPUT);
  pinModeFast(VIBRATOR, OUTPUT);
  digitalWriteFast(VIBRATOR, HIGH);
  initIR();
  
}
/******************************************************************************/
/*                           Update all sensors (except gyro)                 */
/******************************************************************************/
void updateSensors(){
  actuatorUpdate();
  updateIRSensors();
  //updateVibrators();
}

/***********************************************************************/
/*                     Update actuator feeback                         */
/***********************************************************************/
static inline void actuatorUpdate(){
  float volts = ((analogRead(ACTUATORP) / 1023.0) * 5.0) - 0.1;
  actuatorAngle = volts * (90.0 / 3.0);
}
/***********************************************************************/
/*            Recieve data from sensor board (if available)            */
/***********************************************************************/
void updateSensorBoard(){
  // if the sensorBoard has sent us new data
  checkPacket.updateTimer();
  if (checkPacket.timerDone())
  {
  if (sensorBoard.receiveData())
  {
    D0 = fromSensor.data0;
    D1 = fromSensor.data1;
    D2 = fromSensor.data2;
    D3 = fromSensor.data3;
    D4 = fromSensor.data4;
    D5 = fromSensor.data5;
  }
  checkPacket.resetTimer();
  }
}

/***********************************************************************************************/
/*                Take in new values and recalculate current IR sensor values                  */
/*                   Uses data array for filtering, calculates conversion to cm                */
/*                   and selects a single distance per side to believe is true                 */
/*             stored variables are Raw, filtered (ARRAY), perSensor(cm), sideLength(cm)       */
/***********************************************************************************************/
void updateIRSensors(){
  //if the sensor timer has been set (currently greater than 5 ms)
  if (sensorTimer.timerDone())
  {
    //store all of the new raw values from the sensors
    //store the handed values into the raw storage container (they are not really raw
    raw0 = analogRead(IRLONGFRONT);
    raw1 = analogRead(IRLONGBACK);
    raw2 = analogRead(IRLONGLEFT);
    raw3 = analogRead(IRLONGRIGHT);
    raw4 = D4;
    raw5 = analogRead(IRFRONTLEFT);
    raw6 = analogRead(IRFRONTRIGHT);
    raw7 = D3;
    raw8 = D2;
    raw9 = D0;
    raw10 = D1;

    //for accurate timing of our system (we know that the next value is available in 5ms) count from now
    sensorTimer.resetTimer();

    //use the raw unfiltered by us here (analogReads) to calculate a new
    // indexed value to store in our buffered/averaged array
    // the other sensors are being processed where they are taken in
    d0[index] = validRead(raw3, avg0);
    d1[index] = validRead(raw2, avg1);
    d2[index] = validRead(raw0, avg2);
    d3[index] = validRead(raw1, avg3);
    d4[index] = validRead(raw5, avg4);
    d5[index] = validRead(raw6, avg5);

    //calculate a new average for each of these arrays store them
    avg0 = mean(d0, SIZE);
    avg1 = mean(d1, SIZE);
    avg2 = mean(d2, SIZE);
    avg3 = mean(d3, SIZE);
    avg4 = mean(d4, SIZE);
    avg5 = mean(d5, SIZE);

    //Do long IR calcs constraining to just outside of ranges
    IRLR = constrain(calcLongIR((float)avg0, 0), 0, 600);     //store the calculated and constrained
    if (IRLR == 600) IRLR = 0;                        //set a maximum range is equal to 0
    IRLL = constrain(calcLongIR((float)avg1, 0), 0, 600);     //store the calculated and constrained
    if (IRLL == 600) IRLL = 0;                        //set a maximum range is equal to 0
    IRLF = constrain(calcLongIR((float)avg2, 0), 0, 600);     //store the calculated and constrained
    if (IRLF == 600) IRLF = 0;                        //set a maximum range is equal to 0
    IRLB = D5;//constrain(calcLongIR((float)avg3, 0), 0, 600);     //store the calculated and constrained
    if (IRLB == 600) IRLB = 0;                        //set a maximum range is equal to 0

      //do mid IR calcs constraing to just outside of ranges
    IRMB = constrain(calcMidIR(D2), 0, 80);        //store the calculated and constrained
    IRML = constrain(calcMidIR(D3), 0, 80);        //store the calculated and constrained
    IRMR = constrain(calcMidIR(D4), 0, 80);        //store the calculated and constrained

    //do back stereo IR calc and constraining
    IRSBL = constrain(calcBackStereo(D0), 0, 30);      //store the calculated and constrained
    if (IRSBL == 30) IRSBL = 0;                         //set a maximum range is equal to 0
    IRSBR = constrain(calcBackStereo(D1), 0, 30);      //store the calculated and constrained
    if (IRSBR == 30) IRSBR = 0;                         //set a maximum range is equal to 0

      //do front stereo IR calc and constraining
    IRSFL = constrain(calcFrontStereo(avg4, 1), 0, 150); //store the calculated and constrained
    if (IRSFL == 150) IRSFL = 0;                       //set a maximum range is equal to 0
    IRSFR = constrain(calcFrontStereo(avg5, 2), 0, 150); //store the calculated and constrained
    if (IRSFR == 150) IRSFR = 0;                       //set a maximum range is equal to 0

      //calculate the selections of each side (narrow down to a single value per side intelligently)

    if (frontS == 999)                                  // if the value is not initialized
      frontS = avgF(IRLF, IRSFL, IRSFR);                  //run averaging system to get ranged data
    else                                                //else
    frontS = selectFront(IRLF, IRSFL, IRSFR, frontS);   //update the system via smarter algorithm

    if (backS == 999)                                       // if the value is not initialized
      backS = avgB(IRLB, IRMB, IRSBL, IRSBR);                 //run averaging system to get ranged data
    else                                                    //else
    backS =  selectBack(IRLB, IRMB, IRSBL, IRSBR, backS);   //update the system via smarter algorithm

    if (leftS == 999)                              //if the value is not initialized
      leftS = avgS(IRLL, IRML);                      //run averaging system to get ranged data
    else                                           //else
    leftS =   selectSide(IRLL, IRML, leftS);       //update the system via smarter algorithm

    if (rightS == 999)                               // if the value is not initialized
      rightS = avgS(IRLR, IRMR);                       //run averaging system to get ranged data
    else                                             //else
    rightS =  selectSide(IRLR, IRMR, rightS);        //update the system via smarter algorithm

    index += 1; // advance to the next position in the array:

    if (index >= SIZE)   // if we're at the end of the array...
      index = 0;         // ...wrap around to the beginning:

  }
  sensorTimer.updateTimer();
}

/********************************************************************/
/*                Calculate IR data from raw values                 */
/********************************************************************/
/*    These methods take in raw IR data and convert them to cm      */
/*Should (possibly) be upgraded in order to return out of range flag*/
/********************************************************************/

//----------Long IR calculation-(100-550)(~75-600?)------------------
float calcLongIR(float rawValue, int sensor){
  float cm;
  switch (sensor) {
  case RIGHTLONG:

    cm = (-0.000009 * rawValue * rawValue * rawValue) + (0.0163 * rawValue * rawValue) - (9.8377 * rawValue) + 2106.9;
    //50000000 * pow(rawValue, -2.062);
    break;
  case LEFTLONG:
    cm = (-0.000008 * rawValue * rawValue * rawValue) + (0.0152 * rawValue * rawValue) - (9.3264 * rawValue) + 2044.2;
    //50000000 * pow(rawValue, -2.079);
    break;
  case FRONTLONG:

    cm = (0.0042 * rawValue * rawValue) - (4.467 * rawValue) + 1347.4;
    //40000000 * pow(rawValue, -2.05);
    break;
  default:
    float mv = ((rawValue * 0.00488) - 1.091) * 1000;
    cm = 142857.1429 * (1 / mv) + 16;
    break;
  }
  return cm;
}

//----------------Mid IR calculation (10-80cm)------------------------
float calcMidIR(float rawValue){
  if (rawValue > 75)  
    return ((4800 / (rawValue - 20)) + 14);  
  else
    return 100;

}

//----------------Mid IR calculation (20-150cm)-----------------------
float calcFrontStereo(float rawValue, int sensor){
  float cm;
  if (sensor == 1) //left
  {
    cm = 10311 * pow(rawValue, -0.943);
  }
  else if (sensor == 2) //right
  {
    cm = 7335.6 * pow(rawValue, -0.889);
  }
  else
  {
    cm = 9462 / (rawValue - 16.92);
  }
  return cm;
}
//----------------Short IR calculation (4-30cm)------------------------
float calcBackStereo(float rawValue){
  float cm = 2076 / (rawValue - 11);
  return cm;
}

/********************************************************************/
/*                Validate incoming raw value method                */
/********************************************************************/
/*    This method is used to vaLidate incomming raw data            */
/*                                                                  */
/*               Data is being buffered in an Array                 */
/*     The average of that array is handed here with an incoming    */
/*        data signal, this system returns a percentage of change   */
/*            to be recorded into the newest Array index            */
/*                                                                  */
/*        by doing this signal "disBelief" the signal has           */
/*  less of an impact on the stored raw value used for calculation  */
/*                                                                  */
/*      unfortunately I belive this method will be drastiKally      */
/*           changed to allow for a smarter filtering system        */
/*       based on assumed movement/other mathematIcally elegancy    */
/*                                                                  */
/*     although movement based may happen not in the raw value      */
/*         but in the scaled value representation                   */
/********************************************************************/
int validRead(int signal, int average){
  int signalDifference = signal - average;
  return average + (signalDifference * 0.98);//.85 //0.50
}
/*******************************************************************/
/*                         avgSide functions                       */
/*******************************************************************/
/*             used to caLculate an aquisition IR reading          */
/*           performed at roBot init for grabbIng a valid range    */
/*             so that the normal IR average system taKes over     */
/*                                                                 */
/*        if the average fails to find any valid IR readings       */
/*            the value stays at 999 until a range is found        */
/*******************************************************************/
//---------------Average the front sensors for init----------------*/
int avgF(int L, int M1, int M2)                                  //*/
{ //*/
  int total = 0;                                                 //*/
  int div = 0;
  if ((midFCheck(M1)) || (midFCheck(M2))) //*/
  {
    if (midFCheck(M1))    //if mid range looks valid                 */
    { //*/
      total += M1;        //add to total                             */
      div += 1;          //update how many we accepted               */
    }                                                              //*/
    if (midFCheck(M2))    //if mid looks valid                       */
    { //*/
      total += M2;        //add to total                             */
      div += 1;         //update how many we accepted                */
    }
  }
  else if (longCheck(L))    //if long range looks valid                 */
  { //*/
    total += L;        //add to track total                        */
    div += 1;           //update how many we accepted              */
  }                                                              //*/
  //*/
  if (div != 0)      //as long as we got at least one value in     */
  { //*/
    return total / div;  // average our value                      */
  }                                                              //*/
  return 999;            //else remind us to check again           */
}                                                                //*/
//                                                                 */
//-----------------Average the back sensors for init-----------------
int avgB(int L, int M, int S1, int S2)                           //*/
{ //*/
  int total = 0;                                                 //*/
  int div = 0;                                                   //*/
  if (midCheck(M) || shortCheck(S1) || shortCheck(S2))
  { //*/
    if (midCheck(M))    //if mid range looks valid                   */
    { //*/
      total += M;        //add to total                              */
      div += 1;          //update how many we accepted               */
    }                                                              //*/
    if (shortCheck(S1))    // if short range looks valid             */
    { //*/
      total += S1;        //add to total                             */
      div += 1;         //update how many we accepted                */
    }                                                              //*/
    if (shortCheck(S2))    // if short range looks valid             */
    { //*/
      total += S2;        //add to total                             */
      div += 1;         //update how many we accepted                */
    }
  }  //*/
  else if (longCheck(L))    //if long range looks valid                 */
  { //*/
    total += L;        //add to track total                        */
    div += 1;           //update how many we accepted              */
  }
  if (div != 0)      //as long as we got at least one value in     */
  { //*/
    return total / div;  // average our value                      */
  }                                                              //*/
  return 999;            //else remind us to check again           */
}                                                                //*/
//                                                                 */
//-----------------average a side sensor array---------------------*/
int avgS(int L, int M)
{ //*/
  int total = 0;                                                 //*/
  int div = 0;
  if (midCheck(M))    //if mid looks valid                          */
  { //*/
    total += M;        //add to total                              */
    div += 1;         //update how many we accepted                */
  }      //*/
  else if (longCheck(L))    //if long range looks valid                 */
  { //*/
    total += L;        //add to track total                        */
    div += 1;           //update how many we accepted              */
  }                                                              //*/
  //*/
  if (div != 0)      //as long as we got at least one value in     */
  { //*/
    return total / div;  // average our value                      */
  }                                                              //*/
  return 999;            //else remind us to check again           */
}                                                                //*/



/*********************************************************************************************************/
/***********************************IR Selections Methods (per side)**************************************/
/*********************************************************************************************************/
/*      System essentially has memory of where it beLieved the values of the sensors to be before        */
/*          It takes all of the new readings and compares them to that previous value,                   */
/*            first decIding which order to check in based on last stored magnitude                      */
/*        then validating the range of the reading and finally comparing it to the last value            */
/*         to verify it is within the specified error tolerance (currently 30cm (2-26-14)                */
/*                                                                                                       */
/*      These algorithms are going to require review and eKpansion/subtraction to provide the            */
/*        cleanest and most reliaBle signal processing                                                   */
/*********************************************************************************************************/

//----------------------------------------Front IR processing--------------------------------------------------
int selectFront(int Long, int midL, int midR, int prev){
  int check = avgF(IRLF, IRSFL, IRSFR);
  if (varIsAbout(check, prev, 25))  
    return check;
  
  else if (prev > 165)  {                                                                                   //if our last value was out of the range of the mid IRs
    if (varIsAbout(Long, prev, updateRange) && longCheck(Long))                                           // if the long distance is roughly what we had last time
      return Long;                                                                                                   //use the Long sensor reading
    else if (midFCheck(midR) && midFCheck(midL) && varIsAbout(((midL + midR) / 2), prev, updateRange))        //else if the mids look good
      return ((midL + midR) / 2);                                                                              //use the average of the mid sensors
    else                                                                                                    //else the sensors are dirty or out of range
      return prev;                                                                                             //so use the old value for consistency
  }
  else {                                                                                                //since our last value was supposed to be in range of mid IRs
    if (varIsAbout(midL, midR, updateRange) && midFCheck(midR) && midFCheck(midL))                   //if the mid IRs agree within angles and prev value
      return ((midL + midR) / 2);                                                                                      //use the average of the mid sensors
    else if (longCheck(Long) && varIsAbout(Long, prev, updateRange * 2))                                                 //and the long range looks okay
      return Long;                                                                                                     //use the long range sensor
    else                                                                                                          //else the long was dirty/mids were dirty
    return prev;                                                                                                    //so use old value
  }
}
//------------------------------------- Side IR Processing Selection -----------------------------------------
int selectSide(int LongS, int midS, int prev){
  int check = avgS(LongS, midS);
  if (varIsAbout(check, prev, 25))
    return check;
  else if (prev > 100)                                                 //if last reading was out of mid range view
  {
    if (longCheck(LongS) && varIsAbout(LongS, prev, updateRange))         //and the long value resembles and is valid
      return LongS;                                                                  //use the long range sensor
    else if (midCheck(midS) && varIsAbout(midS, prev, updateRange))           // if dirty or out of range and mid looks good
      return midS;                                                                  //use the mid range
    else                                                                      //if all were dirty or not in range or look like jumk
    return prev;                                                                  //leave the value alone
  }
  else                                                           //else last value was in range of mid IRs
  {
    if (midCheck(midS) && varIsAbout(midS, prev, updateRange))             // if mid looks good
      return midS;                                                              //use the mid range
    else if (longCheck(LongS) && varIsAbout(LongS, prev, updateRange * 2)) //else if the long value resembles and is valid
      return LongS;                                                             //use the long range sensor
    else                                                                   //if all were dirty or not in range or look like jumk
    return prev;                                                              //leave the value alone
  }
}


//----------------------------------Back IR update smarts----------------------------------------------
int selectBack(int LongB, int midB, int shortL, int shortR, int prev)
{
   if (varIsAbout((shortL + shortR) / 2, midB, 2) && midB > 6 && shortL > 3 && shortR > 3)  //OVERRIDE NORMAL SYSTEM-- if the shortRanges are confident
  {
    return (midB + shortL + shortR) / 3;                                                          //use them (keeps the system happy to go down to range( not good for effcient tuning))
  }
  int check = avgB(LongB, midB,shortL,shortR);
  if (varIsAbout(check, prev, 25))
    return check;
  else if (prev > 100)                                                                                     //if last reading was out of mid range view
  {
    if (longCheck(LongB) && varIsAbout(LongB, prev, updateRange))                                       //and the long value resembles and is valid
      return LongB;                                                                                                //use the long range sensor
    else if (midCheck(midB) && varIsAbout(midB, prev, updateRange))                                         // if dirty or out of range and mid looks good
      return midB;                                                                                                  //use the mid range
    else                                                                                                    //if all were dirty or not in range or look like jumk
    return prev;                                                                                                      //leave the value alone
  }
  else if (prev > 25)                                                                                   //else if last value was in range of mid IRs (outside of short ish)
  {
    if (midCheck(midB) && varIsAbout(midB, prev, updateRange))                                                     // if mid looks good
      return midB;                                                                                                    //use the mid range
    else if (shortCheck(shortL) && shortCheck(shortR) && varIsAbout(((shortL + shortR) / 2), prev, updateRange))  //else if short looks good
      return (shortL + shortR) / 2;                                                                                    //use the average
    else if (longCheck(LongB) && varIsAbout(LongB, prev, updateRange))                                             //else if the long value resembles and is valid
      return LongB;                                                                                                  //use the long range sensor
    else                                                                                                          //if all were dirty or not in range or look like jumk
    return prev;                                                                                                //leave the value alone
  }
  else if (prev < 25)                                                                                      //else if the last value was in short range
  {
    if (shortCheck(shortL) && shortCheck(shortR) && varIsAbout(((shortL + shortR) / 2), prev, updateRange))      //if shorts look good !!together!! <--- might be bad way to check due to angle (but mid should see any range that could happen
      return (shortL + shortR) / 2;                                                                                        //return average of shorts
    else if (midCheck(midB) && varIsAbout(midB, prev, updateRange))                                              //else if mid looks good
      return midB;                                                                                                        //use the mid range
    else if (longCheck(LongB) && varIsAbout(LongB, prev, updateRange))                                           //else ifthe long value resembles and is valid
      return LongB;                                                                                                  //use the long range sensor
    else                                                                                                         //else all were dirty or not in range or look like jumk
    return prev;                                                                                                          //leave the value alone
  }
}

/**************** Variable is roughly equal method ***********************/
/*       Takes in a variable and a value and an acceptable range (+-)    */
/*     it returns whether that variable is within this range             */
/*                                                                       */
/* NOTE:     if you hand this method a value of 30 for range             */
/*     it is checking 30 above and 30 below currently (ranges 60)        */
/*************************************************************************/
//*/
bool varIsAbout(int var, int val, int plusMinus){ //*/
  return ((var < val + plusMinus) && (var > val - plusMinus));         //*/
}                                                                      //*/
//************************************************************************/

/**************************** Sensor value check methods (per side)***************************/
/*                   Validates whether or not the variable passed is within the              */
/*                        hardcoded ranges for each sensor specifically                      */
/*                                                                                           */
/*          NOTE:     these values may be changed to allow alternate operation               */
/*********************************************************************************************/
//                                                                                           */
//--------------------------Validate a mid range (20-150cm) sensor---------------------------*/
bool midFCheck(int MF)  //check 20-150                                                       */
{ //*/
  return (MF > 20 && MF < 150);                                                            //*/
}                                                                                          //*/
//                                                                                           */
//------------------------Validate a long range (100-550cm) sensor---------------------------*/
bool longCheck(int L)     //check 75-550                                                     */
{ //*/
  return (L < 550 && L > 100);                                                              //*/
}                                                                                          //*/
//                                                                                           */
//--------------------------Validate a mid range (10-80cm) sensor----------------------------*/
bool midCheck(int M)  //check 10-80                                                          */
{ //*/
  return (M > 10 && M < 80);                                                                //*/
}                                                                                          //*/
//                                                                                           */
//-------------------------Validate a short range (4-30cm) sensor----------------------------*/
bool shortCheck(int S) //check 2-30                                                        //*/
{ //*/
  return (S > 2 && S < 30);                                                                //*/
}//                                                                                          */
//                                                                                           */
//********************************************************************************************/

void IRinputInit(){
  //----IR ANALOG INPUT INITIALIZATION----
  pinMode(IRLONGRIGHT, INPUT);
  pinMode(IRLONGLEFT,  INPUT);
  pinMode(IRLONGBACK,  INPUT);
  pinMode(IRLONGFRONT, INPUT);
  //
  //pinMode(IRMIDBACK, INPUT);
  //pinMode(IRMIDLEFT, INPUT);
  //pinMode(IRMIDRIGHT, INPUT);
  //
  //pinMode(IRBACKLEFT, INPUT);
  //pinMode(IRBACKRIGHT, INPUT);
  pinMode(IRFRONTLEFT, INPUT);
  pinMode(IRFRONTRIGHT, INPUT);
}
void IRoutputPowerInit()
{
  //----IR POWER CONTROL INITIALIZATION-----
  //pinMode(IRLONGRIGHTPOWER,OUTPUT);
  //pinMode(IRLONGLEFTPOWER,OUTPUT);
  //pinMode(IRLONGBACKPOWER,OUTPUT);
  //pinMode(IRLONGFRONTPOWER,OUTPUT);

  //pinMode(IRMIDBACKPOWER,OUTPUT);
  //pinMode(IRMIDLEFTPOWER,OUTPUT);
  //pinMode(IRMIDRIGHTPOWER,OUTPUT);

  //pinMode(IRSTEREOBACKLEFTPOWER,OUTPUT);
  //pinMode(IRSTEREOBACKRIGHTPOWER,OUTPUT);
  //pinMode(IRSTEREOFRONTLEFTPOWER,OUTPUT);
  //pinMode(IRSTEREOFRONTRIGHTPOWER,OUTPUT);
}

//void turnOnIR(int IRnumba)
//{
//  switch (IRnumba)
//  {
//    //TURN ON ALL LONG RANGE SENSORS
//  case IRLONGPOWER:
//    digitalWriteFast(IRLONGRIGHTPOWER,HIGH);
//    digitalWriteFast(IRLONGLEFTPOWER,HIGH);
//    digitalWriteFast(IRLONGFRONTPOWER,HIGH);
//    digitalWriteFast(IRLONGBACKPOWER,HIGH);
//    break;
//    //TURN ON ALL MIDRANGE SENSORS
//  case IRMIDPOWER:
//    digitalWriteFast(IRMIDLEFTPOWER,HIGH);
//    digitalWriteFast(IRMIDRIGHTPOWER,HIGH);
//    digitalWriteFast(IRMIDBACKPOWER,HIGH);
//    break;
//    //TURN ON STEREO LEFTS
//  case IRSTEREOPOWER1:
//    digitalWriteFast(IRSTEREOFRONTLEFTPOWER,HIGH);
//    digitalWriteFast(IRSTEREOBACKLEFTPOWER,HIGH);
//    break;
//    //TURN ON STEREO RIGHTS
//  case IRSTEREOPOWER2:
//    digitalWriteFast(IRSTEREOFRONTRIGHTPOWER,HIGH);
//    digitalWriteFast(IRSTEREOBACKRIGHTPOWER,HIGH);
//    break;
//  default:
//    if (IRnumba>0 && IRnumba<12)
//    {
//      digitalWriteFast(IRnumba,HIGH);
//    }
//    break;
//
//  }
//}
//void turnOffIR(int IRnumba)
//{
//  switch (IRnumba)
//  {
//  case IRLONGPOWER:
//    digitalWriteFast(IRLONGRIGHTPOWER,LOW);
//    digitalWriteFast(IRLONGLEFTPOWER,LOW);
//    digitalWriteFast(IRLONGFRONTPOWER,LOW);
//    digitalWriteFast(IRLONGBACKPOWER,LOW);
//    break;
//  case IRMIDPOWER:
//    digitalWriteFast(IRMIDLEFTPOWER,LOW);
//    digitalWriteFast(IRMIDRIGHTPOWER,LOW);
//    digitalWriteFast(IRMIDBACKPOWER,LOW);
//    break;
//  case IRSTEREOPOWER1:
//    digitalWriteFast(IRSTEREOFRONTLEFTPOWER,LOW);
//    digitalWriteFast(IRSTEREOBACKLEFTPOWER,LOW);
//    break;
//  case IRSTEREOPOWER2:
//    digitalWriteFast(IRSTEREOFRONTRIGHTPOWER,LOW);
//    digitalWriteFast(IRSTEREOBACKRIGHTPOWER,LOW);
//    break;
//  default:
//    if (IRnumba>0 && IRnumba<12)
//    {
//      digitalWriteFast(IRnumba,LOW);
//    }
//    break;
//
//  }
//}
////This method will collect the IR data from around the robot and then
//// it will store the number calculated to be in cms
//static inline void updateIR(int IRnumba)
//{
//  switch (IRnumba)
//  {
//    //--------------------------------------------
//  case IRLONGRIGHT:
//    turnOnIR(IRLONGRIGHTPOWER);
//    delay(25);
//    IRLR = analogRead(IRLONGRIGHT);
//    turnOffIR(IRLONGRIGHTPOWER);
//    break;
//    //--------------------------------------------
//  case IRLONGLEFT:
//    turnOnIR(IRLONGLEFTPOWER);
//    delay(25);
//    IRLL=  analogRead(IRLONGLEFT);
//    turnOffIR(IRLONGLEFTPOWER);
//    break;
//    //--------------------------------------------
//  case IRLONGFRONT:
//    turnOnIR(IRLONGFRONTPOWER);
//    delay(25);
//    IRLF=  analogRead(IRLONGFRONT);
//    turnOffIR(IRLONGFRONTPOWER);
//    break;
//    //--------------------------------------------
//  case IRLONGBACK:
//    turnOnIR(IRLONGBACKPOWER);
//    delay(25);
//    IRLB=  analogRead(IRLONGBACK);
//    turnOffIR(IRLONGBACKPOWER);
//    break;
//    //--------------------------------------------
//  case IRMIDLEFT:
//    turnOnIR(IRMIDLEFTPOWER);
//    delay(25);
//    IRML=  analogRead(IRMIDLEFT);
//    turnOffIR(IRMIDLEFTPOWER);
//    break;
//    //--------------------------------------------
//  case IRMIDRIGHT:
//    turnOnIR(IRMIDRIGHTPOWER);
//    delay(25);
//    IRMR=  analogRead(IRMIDRIGHT);
//    turnOffIR(IRMIDRIGHTPOWER);
//    break;
//    //--------------------------------------------
//  case IRMIDBACK:
//    turnOnIR(IRMIDBACKPOWER);
//    delay(25);
//    IRMB=  analogRead(IRMIDBACK);
//    turnOffIR(IRMIDBACKPOWER);
//    break;
//    //--------------------------------------------
//  case IRSTEREOFRONTLEFT:
//    turnOnIR(IRSTEREOFRONTLEFTPOWER);
//    delay(25);
//    IRSFL= analogRead(IRSTEREOFRONTLEFT);
//    turnOffIR(IRSTEREOFRONTLEFTPOWER);
//    break;
//    //--------------------------------------------
//  case IRSTEREOFRONTRIGHT:
//    turnOnIR(IRSTEREOFRONTRIGHTPOWER);
//    delay(25);
//    IRSFR= analogRead(IRSTEREOFRONTRIGHT);
//    turnOffIR(IRSTEREOFRONTRIGHTPOWER);
//    break;
//    //--------------------------------------------
//  case IRSTEREOBACKLEFT:
//    turnOnIR(IRSTEREOBACKLEFTPOWER);
//    delay(25);
//    IRSBL= analogRead(IRSTEREOBACKLEFT);
//    turnOffIR(IRSTEREOBACKLEFTPOWER);
//    break;
//    //--------------------------------------------
//  case IRSTEREOBACKRIGHT:
//    turnOnIR(IRSTEREOBACKRIGHTPOWER);
//    delay(25);
//    IRSBR= analogRead(IRSTEREOBACKRIGHT);
//    turnOffIR(IRSTEREOBACKRIGHTPOWER);
//    break;
//  }
//}
//
//static inline void updateIRs()
//{
//  //--------------------------------------------
//  //Turn on the LONGIRPOWER
//  turnOnIR(IRLONGPOWER);
//  //according to data sheet after power on the sensor will be unstable for 25 ms
//  delay(25);
//  //read all long values (100CM -550CM)
//  IRLR = analogRead(IRLONGRIGHT);
//  IRLL=  analogRead(IRLONGLEFT);
//  IRLF=  analogRead(IRLONGFRONT);
//  IRLB=  analogRead(IRLONGBACK);
//  //turn off the power to the IR array
//  turnOffIR(IRLONGPOWER);
//  //--------------------------------------------
//  //turn on the mid range powered IRs
//  turnOnIR(IRMIDPOWER);
//  //according to data sheet after power on the sensor will be unstable for 50 ms
//  delay(50);
//  //10-80cm
//  IRML=  analogRead(IRMIDLEFT);
//  IRMR=  analogRead(IRMIDRIGHT);
//  IRMB=  analogRead(IRMIDBACK);
//  //turn them off
//  turnOffIR(IRMIDPOWER);
//  //--------------------------------------------
//  //turn on the next set of IRs
//  turnOnIR(IRSTEREOPOWER1);
//  //according to data sheet after power on the sensor will be unstable for 50 ms
//  delay(50);
//  //20-150cm
//  IRSFL= analogRead(IRSTEREOFRONTLEFT);
//  //4-30cm
//  IRSBL= analogRead(IRSTEREOBACKLEFT);
//  //turn them off
//  turnOffIR(IRSTEREOPOWER1);
//  //--------------------------------------------
//  //turn on the final set of IRs
//  turnOnIR(IRSTEREOPOWER2);
//  //according to data sheet after power on the sensor will be unstable for 50 ms
//  delay(50);
//  //20-150cm
//  IRSFR= analogRead(IRSTEREOFRONTRIGHT);
//  //4-30
//  IRSBR= analogRead(IRSTEREOBACKRIGHT);
//  turnOffIR(IRSTEREOPOWER2);
//}

//static inline void updateVibrators()
//{
//  if(actuatorAngle > 70)
//    {digitalWriteFast(VIBRATOR, LOW);}
//  else
//    {digitalWriteFast(VIBRATOR, HIGH);}
//}





