//Gyroscope.h
//This header file will handle all of the necessary code to
//  instruct the gyroscope to come online and hand us
//  gyroscope Z axis data to execute turning commands elsewhere
//
//  Gyroscope will be initialized at board initialize
//       and be handed an I2C address at init
//
//     It will then end the sleep mode (set at power on)
//        and prepare the necessary variables to begin monitoring data
//
//    The data will regularly be check and automatically keep track of the
//      current angle of the sensor (integrating the radial acceleration)
//
//   Methods include
//     void initializeGyro(address)
//     void calibrateZero(length)
//   --Takes a number of samples (length) and compares them to establish a
//   --deadzone and a biased offset (from zero)
//     int  currentAngle()
//   --Returns the current internal integrated angle to date (since last wipe)
//     void zeroAngle()
//   --Wipes the current angle integrated thus far (clean state after wipe)
//     void updateGyro()




//Main loop ran from communications.ino
//Handles updating the gyro information through I2C device and storing data in register for other systems to use
inline void updateGyro()
{
  now = millis();
  if (abs(now - lastTime) > 50)
  {
    //store the old value of the raw
    compare = zRaw;

    //calculate the time between samples for integrating
    dt = ((now - lastTime) / 1000);

    //take in new value minus the offset calibration
    zRaw = sampleGyroZ() - offset;

    //store time of this sample
    lastTime = millis();

    //If the point taken in is not outside of the deadzone
    if (((zRaw > -(z_dead_g / 2)) && (zRaw < (z_dead_g / 2))) || abs(zRaw - compare) > 1000)
      //make it a pretty zero
      zFilt = 0;
    else
      //store the value that was outside the deadzone
      zFilt = zRaw / 16.4;

    latestFilteredSample = zFilt;
    //Integrate to store angle
    zRot += zFilt * dt;

    //calculate the robot turning angle (based on how much it has changed since last update)
    //can work independantly of the calculation integral
    macroAngle += zRot - robotHeading;

    //store current angle
    robotHeading = zRot;
  }
}

// --------------------------------------------------------
// MPU6050_read
int MPU6050_read(int starts, uint8_t *buffer, int sized)
{
  int i, n, error;

  Wire.beginTransmission(MPU6050_I2C_ADDRESS);
  n = Wire.write(starts);
  if (n != 1)
    return (-10);

  n = Wire.endTransmission(false);    // hold the I2C-bus
  if (n != 0)
    return (n);

  // Third parameter is true: relase I2C-bus after data is read.
  Wire.requestFrom(MPU6050_I2C_ADDRESS, sized, true);
  i = 0;
  while (Wire.available() && i < sized)
  {
    buffer[i++] = Wire.read();
  }
  if ( i != sized)
    return (-11);

  return (0);  // return : no error
}

// --------------------------------------------------------
// MPU6050_write
int MPU6050_write(int starts, const uint8_t *pData, int sized)
{
  int n, error;
  Wire.beginTransmission(MPU6050_I2C_ADDRESS);
  n = Wire.write(starts);        // write the start address
  if (n != 1)
    return (-20);

  n = Wire.write(pData, sized);  // write data bytes
  if (n != sized)
    return (-21);

  error = Wire.endTransmission(true); // release the I2C-bus
  if (error != 0)
    return (error);
  return (0);         // return : no error
}

// --------------------------------------------------------
// MPU6050_write_reg
static int MPU6050_write_reg(int reg, uint8_t data)
{
  int error;
  error = MPU6050_write(reg, &data, 1);
  return (error);
}

//Method used to average a set up data
int16_t averageSet(int numsamples, int16_t sampled[]) {
  long temp;
  temp = 0;
  for (int i = 0; i < numsamples; i++) {
    temp = temp + sampled[i];
  }
  return temp / numsamples;
}

//Take a single sample from the gyro Z axis
inline int sampleGyroZ()
{
  MPU6050_read (MPU6050_GYRO_ZOUT_H, (uint8_t *) &gyro, sizeof(gyro));
  SWAP (gyro.reg.z_gyro_h, gyro.reg.z_gyro_l);
  return gyro.value.z_gyro;
}

//Run a calibration of a certain length to ensure deadzone and offset from zero
void calibrateZero(int length)
{

  for (int i = 0; i < length; i++)
  {
    //TAKE A BUNCHA SAMPLES
    samples[i] = sampleGyroZ();

    //AFTER THE FIRST SAMPLE
    //CHECK TO SEE IF THE DIFFERENCE BETWEEN THE CALIBRATION
    //SAMPLES IS LARGER THAN 1000... IF SOO.. STORE PREVIOUS
    //SAMPLE INTO THIS SAMPLE// (AVOIDING JUMPS AND BUMPS)
    if ((i > 1) && (abs(samples[i] - samples[i - 1]) > 1000))
    {
      //STORE PREVIOUS VALUE
      samples[i] = samples[i - 1];
    }
    //Serial.println(samples[i], DEC);
    delay(5);
  }

  //Get the offset value by averaging the set of samples
  offset = averageSet(length, samples);

  //Serial.println(offset, DEC);

  //SET the high and low values to the offset center
  high = offset;
  low = offset;

  //Run through the samples again to determine the greatest
  //fluctuations from the center point
  for (int i = 0; i < length; i++)
  {
    if (samples[i] > high) {
      high = samples[i];
    }
    if (samples[i] < low) {
      low = samples[i];
    }
  }

  //The established deadzone is the peak to peak measured
  //noise on the signal line
  z_dead_g = high - low;

  //an attempt to allow for updating to resume without timer error
  lastTime = millis();
}

//Init the gyro by removing the sleep bit and ensuring the sensativity setting
void initializeGyro(word address)
{
  // Clear the 'sleep' bit to start the sensor.
  MPU6050_write_reg(MPU6050_PWR_MGMT_1, 0);
  //Set Gyro Sensativity
  MPU6050_write_reg(MPU6050_GYRO_CONFIG, MPU6050_FS_SEL_3); //+-2000deg/sec (/16.4)
  // delay(10000);
  delay(1500);
  calibrateZero(500);
}

//wipes the internal integrated value to zero for integrity.
void zeroInternalAngle()
{
  macroAngle = 0;
}

void headingZero()
{
  zeroInternalAngle();
  robotHeading = 0;
  zRot = 0;
}

