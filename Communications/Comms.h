//comms.h
// initialize comms
void initializeCommunications()
{
  
  Serial.begin(38400);
  Serial1.begin(115200);
  Serial3.begin(57600);
  from_control.begin(details(fromControl), &Serial1);
  to_motor.begin(details(outMotor), &Serial3);
  fromPower.begin(details(power), &Serial3);
  to_control.begin(details(toControl), &Serial1);
  sensorBoard.begin(details(fromSensor), &Serial);  
  
  while (!from_control.receiveData())
  {    
    packetWait();
    delay(60);
  };
  
  readyToSend=true;
  latency.updateTimer();
  
  delay(1);
  flicker75(); 
  commTimer.resetTimer();
 
}

inline void commSafety()
{
  commTimer.updateTimer();
  if (commTimer.timerDone())
  {
    currentState = 0;
    updateLeds();
    outMotor.leftMotorSpeed = 255;
    outMotor.rightMotorSpeed = 255;
    outMotor.actuator = 255;
    to_motor.sendData();
    sendTime.resetTimer();
    while (!from_control.receiveData())
    {
      delay(5);
//      sendTime.updateTimer();
//      if (sendTime.timerDone())
//      {         
//        packetWait();
//      }           
    }   
    
    readyToSend=true;        //make not we got a good one
    latency.resetTimer();  //delay till send after received
    commTimer.resetTimer(); //safety system reset   
  }
}

inline void updateComms()
{
  if (fromPower.receiveData())
  {
    amps = power.amphours;
    volts = power.averageVolts;
    watts = power.watthours;
    toControl.volts = volts;
    toControl.watts = watts;
    toControl.amps = amps;
  }
  //IR Data from sensor board
  updateSensorBoard();
  
  latency.updateTimer();
  if(latency.timerDone()&&readyToSend)
  {
    to_control.sendData();
    readyToSend=false;    
  }
  
  //try to get a good packet from control box
  if (from_control.receiveData())
  {  
    //prepare and send the data (screen data)
    toControl.bucketAngle = actuatorAngle;
    toControl.gyroAngle = macroAngle;
    toControl.robotHeading = robotHeading;
    //LONG IRs
//    toControl.IRRightLong = (int)IRLR;
//    toControl.IRLeftLong = (int)IRLL;
//    toControl.IRBackLong = (int)IRLB;
//    toControl.IRFrontLong = (int)IRLF;
//    //MID IRs
//    toControl.IRBackMid = (int)IRMB;
//    toControl.IRLeftMid = (int)IRML;
//    toControl.IRRightMid = (int)IRMR;
//    //BACK IRs
//    toControl.IRBackL = (int)IRSBL;
//    toControl.IRBackR = (int)IRSBR;
//    //FRONT IRs
//    toControl.IRFrontR = (int)IRSFR;
//    toControl.IRFrontL = (int)IRSFL;
    ////Send along some raw values
    //toControl.IRFrontLongR = raw0;
    //toControl.IRBackLongR = raw1;
    //toControl.IRLeftLongR = raw2;
    //toControl.IRRightLongR = raw3;
    //toControl.IRRightMidR = raw4;
    //toControl.IRFrontLR = raw5;
    //toControl.IRFrontRR = raw6;
    //toControl.IRLeftMidR = raw7;
    //toControl.IRBackMidR = raw8;
    //toControl.IRBackLR = raw9;
    //toControl.IRBackRR = raw10;
    //    toControl.debug1 = frontS;
    //    toControl.debug2 = leftS;
    //    toControl.debug3 = rightS;
    //    toControl.debug4 = backS;
    toControl.F = (int)frontS;
    toControl.L = (int)leftS;
    toControl.R = (int)rightS;
    toControl.B = (int)backS;
    toControl.motorL = outMotor.leftMotorSpeed;
    toControl.motorR = outMotor.rightMotorSpeed;
    //Fire off a packet 
    macroRequest = fromControl.macro_command;   

    //record control box's state
    currentState = fromControl.state;    
    
    //as long as we are NOT doing a macro request
    if (macroRequest == 0)
    {
      //pass motor the controls from control box
      outMotor.leftMotorSpeed = fromControl.leftMotorSpeed;
      outMotor.rightMotorSpeed = fromControl.rightMotorSpeed;
      outMotor.actuator = fromControl.actuator;
      to_motor.sendData();
    }
    else
    {      
      updateMacro();
    }    
    //time stamp activity from control box
    commTimer.resetTimer();
    readyToSend=true;
    latency.resetTimer();
  }
  //failed to get fresh packet
  else
  {
    commSafety();
  }
}



