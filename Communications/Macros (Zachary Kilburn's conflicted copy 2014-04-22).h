//macros.h
//autonomous.h
//3-2-14 STACEE can walk in circles (   almost :)  )

//3-25-14 STACEE can do forward/mine/backward/dump

enum SLOPE {
  POSITIVE = true, NEGATIVE = false
};
SLOPE slope1;
#define POSITIVE true
#define NEGATIVE false
#define BACKWARD false
#define FORWARD true
#define tunable90turnHelper 2

bool peakPresent, targetPresent, slopePositive;
int slopeCalc, lastMeasuredDistance, sideLength, targetTime, peakTime;



//USED FOR ACQUIRING A NEW MINING LOCATION PER DIG
//     <--      -->
//      <-  \|/  ->
//          \|/
//           |
//
//look for peak in side length and then drop
//check for local min max, and determine location of parallel
//follow left or right side assuming facing the end of the collection area roughly 150 cm away
//turning either to the left to compensate or right to comensate

//Right tells which IR to watch as well as information about turning based on which way you start turning
//curve allows easy manipulation of the amount of curvature of the turn
inline bool turn90(bool Right, int curve, bool FB) {
  continueMacro = 0;
  peakPresent = false;
  targetPresent = false;
  slopePositive = POSITIVE;

  //start off rolling at a certain pace of turning (this method assumes that you are not
  //being dumb about the magnitude of each part of the command) (turn rates can get high quickly)--hopefully revised 4-16
  if (Right && FB)
    differentialDrive(((motorLow + motorHigh) / 2), -curve); //turn right
  else if (!Right && FB)
    differentialDrive(((motorLow + motorHigh) / 2), curve); //turn left
  else if (Right && !FB)
    differentialDrive(-((motorLow + motorHigh) / 2), -curve); //backward right
  else
    differentialDrive(-((motorLow + motorHigh) / 2), curve); //backward left

  Timers decisionTimer(50);

  //if you are going forward
  if (FB) {
    //while you are not close to a wall in front and you havent seen a peak or the valley(target)
    while (frontS > 100 && !peakPresent && !targetPresent && continueMacro == 0) {
      updateSensorBoard();
      updateIRSensors();
      decisionTimer.updateTimer();

      //choose side to listen to (would be smart to have a weighted average right here!!
      //weighted average would feed into the PID system within wider time intervals
      if (Right)
        sideLength = leftS;
      else
        sideLength = rightS;
      
      
      if (decisionTimer.timerDone()) {
        //whoops had that in the wrong location!!
        slopeCalc = lastMeasuredDistance - sideLength;
        lastMeasuredDistance = sideLength;
        
        if (peakPresent && slopeCalc > 0) { //slope change negative to positive
          if (targetTime > tunable90turnHelper) {
            targetPresent = true;
            setColor(500, 500, 1200);
          }
          else
            targetTime++;
          continue;
        }
        else if (slopePositive == (slopeCalc > 0)) { //no slope change
          peakTime = 0;
          continue;
        }
        else if (slopeCalc < 0) { //slope change positive to negative
          if (slopePositive != NEGATIVE) {
            if (peakTime > tunable90turnHelper) {
              peakPresent = true;
              setColor(1000, 500, 350);
              slopePositive = NEGATIVE;
            }
            else {
              peakTime++;
            }
          }
          else
            targetTime = 0;
        }
        to_motor.sendData();
        if (from_control.receiveData()) {
          updateScreen();
          continueMacro = fromControl.macro_stop;
        }
      }
    }
  }
  else  {  //BACKWARD CONTROL SECTION
    while (backS < 320 && !peakPresent && !targetPresent && continueMacro == 0) {
      updateSensorBoard();
      updateIRSensors();
      decisionTimer.updateTimer();

      if (Right)
        sideLength = leftS;
      else
        sideLength = rightS;

      slopeCalc = lastMeasuredDistance - sideLength;
      lastMeasuredDistance = sideLength;
      if (decisionTimer.timerDone()) {
        if (peakPresent && slopeCalc > 0) { //slope change negative to positive
          if (targetTime > tunable90turnHelper) {
            targetPresent = true;
            setColor(500, 500, 1200);
          }
          else
            targetTime++;
          continue;
        }
        else if (slopePositive == (slopeCalc > 0)) { //no slope change
          peakTime = 0;
          continue;
        }
        else if (slopeCalc < 0) { //slope change positive to negative
          if (slopePositive != NEGATIVE) {
            if (peakTime > tunable90turnHelper) {
              peakPresent = true;
              setColor(1000, 500, 350);
              slopePositive = NEGATIVE;
            }
            else {
              peakTime++;
            }
          }
          else
            targetTime = 0;
        }
        to_motor.sendData();
        if (from_control.receiveData()) {
          updateScreen();
          continueMacro = fromControl.macro_stop;
        }
      }
    }


  }
  allStop();
  motor_unStick();
  return (continueMacro == 0);
}
//int weighted[20];
//int weightedSensorReading(int weight, int reading) {
//
//
//}
//bool getSideChange(bool Right){
//  if (Right)
//    sideLength=rightS;
//  else
//    sideLength=leftS;
//  while(lastMeasuredDistance!=sideLength) {
//    lastMeasuredDistance=weightedSensorReading(1,Right);
//  }
//
//  slope=lastMeasuredDistance-sideLength;
//  if(slope>0)
//    return POSITIVE;
//  else
//    return NEGATIVE;
//}

//used to push the rock further towards the wall then digging
inline bool pushRock(int distanceF) {
  continueMacro = 0;
  int magnitude;
  float sideCorrect;
  float travelDistanceRemaining = frontS - distanceF;
  PID rSideError(152, sideKpR, sideKiR, sideKdR, 0);
  PID lSideError(152, sideKpL, sideKiL, sideKdL, 0);
  PIDTimer.resetTimer();
  while ((frontS >= distanceF) && continueMacro == 0)  {
    //update all readings before we make decisions (some are throttled in the background with timers)
    updateSensorBoard();
    updateIRSensors();

    //update the PID system timer
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())  //Check if the timer is done
    {
      magnitude = motorLow;
      //determine how to correct travel drifting by checking sides
      sideCorrect = rSideError.updateOutput(rightS) - lSideError.updateOutput(leftS);

      //if the side length is not about 150 correct based on length
      if ((!(varIsAbout(152, leftS, sideDeadL)) || (!(varIsAbout(152, rightS, sideDeadR))))) //note that this catch is more for proportional than for derivative
        differentialDrive(magnitude, sideCorrect);
      else
        variableDrive(magnitude);

      //send motor data
      to_motor.sendData();
      if (from_control.receiveData()) {
        //report to the screen
        updateScreen(lSideError);
        //check for halt command
        continueMacro = fromControl.macro_stop;
      }
    }
  }
  //end command
  allStop();
  motor_unStick();

  //return if halt
  return (continueMacro == 0);
}


//mine routine for auto mode 2
inline bool actuatorMine2()
{
  continueMacro = 0;
  static int mine = 0;
  setColor(1600, 1200, 900);

  actuator(actuatorDrivingAngle);
  betweenMacro();

  if (mine == 1)
    turn90(false, motorHigh - 3, FORWARD);
  else
    turn90(true, motorHigh - 3, FORWARD);
  betweenMacro();

  backupAfterMineMove(120);
  betweenMacro();

  actuator(5);
  betweenMacro();

  drive(5);
  betweenMacro();

  actuator(actuatorDrivingAngle);
  betweenMacro();

  if (mine == 1) {
    turn90(false, motorHigh - 3, BACKWARD);
    mine = 0;
  }
  else {
    mine = 1;
    turn90(true, motorHigh - 3, BACKWARD);
  }
}

//revised auto mode
inline bool straightShot2(bool continuable) {
  static int range = 210;
  continuable = actuator(actuatorDrivingAngle);
  betweenMacro();
  if (firstShot) {
    firstShot = false;
    continuable = followBoth(150);
    betweenMacro();
    continuable = actuator(90);
    betweenMacro();
    continuable = pushRock(83);
    betweenMacro();
    continuable = backupAfterRock(150);

  }
  else {
    if (range > 150)
      range = range - 10;
    else
      range = 250;
    continuable = followBoth(range);
  }
  betweenMacro();

  if (continuable == true)  {
    continuable = actuatorMine2();
    betweenMacro();
  }
  if (continuable == true)  {
    continuable = driveUntilIRRear(75);
    betweenMacro();
  }
  if (continuable == true)  {
    continuable = actuatorDump();
    betweenMacro();
  }
  return (continuable == 1);
}


//mine methods for ver1 auto mode
//-----------------------------------------------------------------------------------
//drop the actuator into mining position
//then pick it back up
inline bool actuatorMine()
{
  setColor(1200, 1600, 0);
  continueMacro = 0;
  actuator(1);
  betweenMacro();
  drive(5);
  betweenMacro();
  actuator(actuatorDrivingAngle);
}



//older version of continuous auto mode
inline bool straightShot(bool continuable) {
  continuable = actuator(actuatorDrivingAngle);
  if (continuable == true)
  {
    if (firstShot) {
      firstShot = false;
      if (followBoth(160))
        if (actuator(90))
          if (followBoth(150))
            continuable = backupAfterRock(200);
          else
            continuable = false;
    }
    else
      continuable = followBoth(160);

    betweenMacro();
  }
  if (continuable == true)  {
    continuable = actuatorMine();
    betweenMacro();
  }
  if (continuable == true)  {
    continuable = driveUntilIRRear(75);
    betweenMacro();
  }
  if (continuable == true)  {
    continuable = actuatorDump();
    betweenMacro();
  }
  return (continuable == 1);
}

inline void initializeMacros() {
  angleSet = 0;
  zeroInternalAngle();
}


//untested
void levelLR() {
  continueMacro = 0;
  PIDTimer.setInterval(20);
  PIDTimer.resetTimer();
  while (!(varIsAbout(IRSFL, IRSFR, 1)) && (continueMacro == 0)) {
    updateSensorBoard();
    updateIRSensors();
    PIDTimer.updateTimer();

    if (PIDTimer.timerDone()) {
      turnHelp((IRSFL - IRSFR) / 5);
      to_motor.sendData();
      if (from_control.receiveData())      {
        updateScreen();
        continueMacro = fromControl.macro_stop;
      }
    }
  }
  //stop the robot after
  allStop();
  motor_unStick();
  PIDTimer.setInterval(50);
}

//appears to function alrgith
inline bool backupAfterMineMove(int distance) {
  setColor(1600, 1200, 0);
  continueMacro = 0;
  int magnitude, sideOutput;
  int targetL = leftS;
  int targetR = rightS;
  PID driveBackward(distance, 0.5, 0, 0, 1);
  PID sideErrorL(targetL, 0, sideKiL, sideKdL, 0);
  PID sideErrorR(targetR, 0, sideKiR, sideKdR, 0);
  driveBackward.clearSystem();
  PIDTimer.resetTimer();

  //while the actuator is not at the target(within 1)
  while ((frontS <= distance) && continueMacro == 0)  {
    updateSensorBoard();
    updateIRSensors();
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())
    {
      //choose how fast to command
      magnitude = driveBackward.updateOutput(frontS);
      sideOutput = sideErrorL.updateOutput(leftS) - sideErrorR.updateOutput(rightS);
      magnitude = constrain(magnitude, motorLow, (motorHigh));


      if ((!(varIsAbout(targetL, leftS, sideDeadL)) || (!(varIsAbout(targetR, rightS, sideDeadR))))) //note that this catch is more for proportional than for derivative
        differentialDrive(-magnitude, sideOutput);
      else
        variableDrive(-magnitude);

      //send motor data
      to_motor.sendData();

      //check for command halt
      if (from_control.receiveData())      {
        continueMacro = fromControl.macro_stop;
        //report to the screen
        updateScreen(driveBackward);
      }
      PIDTimer.resetTimer();
    }
  }
  //end command
  allStop();
  motor_unStick();
  //return if halt
  return (continueMacro == 0);
}

//believe this is working better as a backup function compared to the other drive until ir rear
//-----------------------------------
inline bool backupAfterRock(int distance) {
  setColor(1600, 1200, 0);
  continueMacro = 0;
  int magnitude, sideOutput;
  PID driveBackward(distance, 0.5, 0, 0, 1);
  PID sideErrorL(152, sideKpL, sideKiL, sideKdL, 0);
  PID sideErrorR(152, sideKpR, sideKiR, sideKdR, 0);
  driveBackward.clearSystem();
  PIDTimer.resetTimer();

  //while the actuator is not at the target(within 1)
  while ((frontS <= distance) && continueMacro == 0)  {
    updateSensorBoard();
    updateIRSensors();
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())
    {
      //choose how fast to command
      magnitude = driveBackward.updateOutput(frontS);
      sideOutput = sideErrorL.updateOutput(leftS) - sideErrorR.updateOutput(rightS);
      magnitude = constrain(magnitude, motorLow, (motorHigh));


      if ((!(varIsAbout(152, leftS, sideDeadL)) || (!(varIsAbout(152, rightS, sideDeadR))))) //note that this catch is more for proportional than for derivative
        differentialDrive(-magnitude, sideOutput);
      else
        variableDrive(-magnitude);

      //send motor data
      to_motor.sendData();

      //check for command halt
      if (from_control.receiveData())      {
        continueMacro = fromControl.macro_stop;
        //report to the screen
        updateScreen(driveBackward);
      }
      PIDTimer.resetTimer();
    }
  }
  //end command
  allStop();
  motor_unStick();
  //return if halt
  return (continueMacro == 0);
}
//-----------------------------------------------------------------------------------


//Negating its own weight! needs fixed
inline bool driveUntilIRRear(int distance) {
  setColor(1600, 1200, 0);
  continueMacro = 0;
  int magnitude, sideOutput;
  PID driveBackward(distance, 0.5, 0, 0, 1);
  PID sideErrorL(152, sideKpL, sideKiL, sideKdL, 0);
  PID sideErrorR(152, sideKpR, sideKiR, sideKdR, 0);
  driveBackward.clearSystem();
  PIDTimer.resetTimer();

  //while the actuator is not at the target(within 1)
  while ((backS >= distance) && continueMacro == 0)  {
    updateSensorBoard();
    updateIRSensors();
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())
    {
      //choose how fast to command
      magnitude = driveBackward.updateOutput(backS);
      sideOutput = sideErrorL.updateOutput(leftS) - sideErrorR.updateOutput(rightS);
      magnitude = constrain(magnitude, motorLow, (motorHigh));


      if ((!(varIsAbout(152, leftS, sideDeadL)) || (!(varIsAbout(152, rightS, sideDeadR))))) //note that this catch is more for proportional than for derivative
        differentialDrive(-magnitude, sideOutput);
      else
        variableDrive(-magnitude);

      //send motor data
      to_motor.sendData();

      //check for command halt
      if (from_control.receiveData())      {
        continueMacro = fromControl.macro_stop;
        //report to the screen
        updateScreen(driveBackward);
      }
      PIDTimer.resetTimer();
    }
  }
  //end command
  allStop();
  motor_unStick();
  //return if halt
  return (continueMacro == 0);
}
int revisedOffset;
int revisedCmd1;
int revisedCmd2;

//driving helper method, offset selects balance
//larger offset = greater turning speed -- needs tuned
inline void differentialDrive(int mag, int offset) {

  if (mag > 0) //we are turning the robot to the left (dampen the left motor)
  {
    //choose a revised offset value and constrain it with respect to the commanded value
    //
    revisedOffset = constrain(offset, -(mag * 0.8), (mag * 0.8));
    revisedCmd1 = constrain((mag - (revisedOffset)), -motorHigh * 1.5, motorHigh * 1.5);
    revisedCmd2 = constrain((mag + (revisedOffset)), -motorHigh * 1.5, motorHigh * 1.5);
    if (abs(revisedCmd1) < (motorLow / 2)) {
      revisedCmd1 = motorLow * (revisedCmd1 / abs(revisedCmd1));
    }
    if (abs(revisedCmd2) < (motorLow / 2)) {
      revisedCmd2 = motorLow * (revisedCmd2 / abs(revisedCmd2));
    }
    outMotor.leftMotorSpeed  = revisedCmd1;//0.50
    outMotor.rightMotorSpeed = revisedCmd2;    //0.80
  }
  else          //we are turning the robot to the right (dampen the right motor
  {
    //choose a revised offset value and constrain it with respect to the commanded value
    //
    revisedOffset = constrain(offset, (mag * 0.8), -(mag * 0.8));
    revisedCmd1 = constrain((mag - (revisedOffset)), -motorHigh * 1.5, motorHigh * 1.5);
    revisedCmd2 = constrain((mag + (revisedOffset)), -motorHigh * 1.5, motorHigh * 1.5);
    if (abs(revisedCmd1) < (motorLow / 2)) {
      revisedCmd1 = motorLow * (revisedCmd1 / abs(revisedCmd1));
    }
    if (abs(revisedCmd2) < (motorLow / 2)) {
      revisedCmd2 = motorLow * (revisedCmd2 / abs(revisedCmd2));
    }
    outMotor.leftMotorSpeed = revisedCmd1;          //0.80
    outMotor.rightMotorSpeed = revisedCmd2; //0.50
  }

}

//------------------------------------------------------------------
//when IRs are in place this will approach the wall within 10cm?
//inline void approachDump(){
//  int driveSpeed;
//  PID approach(30,0.5,0,0,0);
//  PIDTimer.resetTimer();
//  continueMacro = 0;
//  while ((backS > 30) && (continueMacro == 0))  {
//    PIDTimer.updateTimer();
//    if(PIDTimer.timerDone()) {
//      //Serial.println();
//      variableDrive(-15);
//      if (from_control.receiveData())      {
//        continueMacro = fromControl.macro_stop;
//        updateScreen();
//      }
//      to_motor.sendData();
//    }
//  }
//}

inline bool followBoth(int distanceF) {
  setColor(1020, 1600, 510);
  continueMacro = 0;
  int magnitude;
  float sideCorrect;
  float travelDistanceRemaining = frontS - distanceF;
  PID rSideError(152, sideKpR, sideKiR, sideKdR, 0);
  PID lSideError(152, sideKpL, sideKiL, sideKdL, 0);
  PIDTimer.resetTimer();
  while ((frontS >= distanceF) && continueMacro == 0)  {
    //update all readings before we make decisions (some are throttled in the background with timers)
    updateSensorBoard();
    updateIRSensors();

    //update the PID system timer
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())  //Check if the timer is done
    {
      //if timer is done make a PID decision
      //calculate the amount of travel forward left and set up speed control
      travelDistanceRemaining = ( frontS - distanceF );
      if (travelDistanceRemaining > slowingDistance)
        magnitude = motorHigh;
      else
        magnitude = constrain((motorHigh - (travelDistanceRemaining / slowingDistance) * motorHigh), motorLow, motorHigh); //calculate speed forward
      //somehow this is getting inverted^^ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      //determine how to correct travel drifting by checking sides
      sideCorrect = rSideError.updateOutput(rightS) - lSideError.updateOutput(leftS);

      //if the side length is not about 150 correct based on length
      if ((!(varIsAbout(152, leftS, sideDeadL)) || (!(varIsAbout(152, rightS, sideDeadR))))) //note that this catch is more for proportional than for derivative
        differentialDrive(magnitude, sideCorrect);
      else
        variableDrive(magnitude);

      //send motor data
      to_motor.sendData();
      if (from_control.receiveData()) {
        //report to the screen
        updateScreen(lSideError);
        //check for halt command
        continueMacro = fromControl.macro_stop;
      }
    }
  }
  //end command
  allStop();
  motor_unStick();

  //return if halt
  return (continueMacro == 0);
}

//************************************MACRO SYSTEM LOOP FOR UPDATES************************************
//-----------Loop that checks for macro information and runs them appropriately-----------------------
inline void updateMacro() {
  if (macroRequest != 0)  {
    initMacroSystem(MACRO);
    //Check to see which macro was requested (macro_command)
    //use the macro_sub_command for turn angle and options

    sub_command = fromControl.macro_sub_command;
    //MACRO REQUESTS (NOT SETTINGS OR AUTO)
    switch (macroRequest) {
        //------Gyroscope macros---------
      case TURN:
        turn(sub_command);
        break;
        //------future home of encoder macros--------
      case DRIVE:
        drive(sub_command);
        break;
        //---------actuator macros-------
      case ACTUATOR:
        switch (sub_command) {
          case 250:
            actuatorDump();
            break;
          case 255:
            actuatorMine();
            break;
          case 145:
            actuatorMine();
            break;
          default:
            if (sub_command < 101 && sub_command > -1)
              actuator(sub_command);
            break;
        }
        break;
        //---------Drive while turning------------
      case 4:
        dDrive(sub_command);
        break;
        //--------------------IR Systems Macros----------------------
      case 5:
        driveUntilIRFront(100);
        break;
      case 6:
        driveUntilIRRear(30);
        break;
      case 7:
        followBoth(100);
        break;
      case 8:
        followLeft(leftS, 100);
        break;
      case 9:
        if (midFCheck(IRSFL) && midFCheck(IRSFR))
          levelLR();
        break;
      case 10:
        //Turn towards distance side (1= short 2=long)
        if (sub_command == 1)
        {
          //turn towards short
        }
        else if (sub_command == 2)
        {
          //turn towards long
        }
        break;
      case 11:
        //- Validate IR Data (scootch)
        break;

      case 68: //ADDITIONAL FEATURES
        updateAutonomousFeatures(sub_command);
        break;
      case 69:  //Autonomous Systems
        //pass command off to the autonomous handler
        autoSet = 1;
        //for now changing with a "handy" variable
        autoState = sub_command;  //sub_command
        updateAutonomous();
        //handy+=1;
        //if (handy>3) handy=0;
        break;
        //Variable manipulation systems;
      case 70:
        motorHigh = sub_command;
        break;
      case 71:
        motorLow = sub_command;
        break;
      case 72:
        PIDTimer.setInterval(sub_command);
        break;
      case 73:
        sideKpL = sub_command;
        break;
      case 74:
        sideKiL = sub_command;
        break;
      case 75:
        sideKdL = sub_command;
        break;
      case 76:
        gyroCorrectL = sub_command;
        break;
      case 77:
        sideDeadL = sub_command;
        break;
      case 78:
        gyroKp = sub_command;
        break;
      case 79:
        gyroKi = sub_command;
        break;
      case 80:
        gyroKd = sub_command;
        break;
      case 81:
        sideKpR = sub_command;
        break;
      case 82:
        sideKiR = sub_command;
        break;
      case 83:
        sideKdR = sub_command;
        break;
      case 84:
        gyroCorrectR = sub_command;
        break;
      case 85:
        sideDeadR = sub_command;
        break;
      case 86:
        slowingDistance = sub_command;
        break;
    }
    finishMacro();
  }
}
inline void updateAutonomousFeatures(int cmd) {
  switch (cmd) {
    case 1:     //Gyro RESET
      headingZero();
      break;
    case 2:    //reInit (IR/Gyro)
      frontS = 999,
      backS = 999,
      leftS = 999,
      rightS = 999;
      calibrateZero(200);
      break;
    case 3: //unstick IR
      frontS = 999,
      backS = 999,
      leftS = 999,
      rightS = 999;
      break;
  }
}


inline void updateAutonomousSystem(int cmd) {
  //figure out what the hell they wanted
}

void betweenMacro() {
  zeroInternalAngle();
  frontS = 999,
  backS = 999,
  leftS = 999,
  rightS = 999;
  Timers AUTOTimer(5);
  for (int i = 0; i < 1000; i++)
  {
    while (!AUTOTimer.timerDone())
    {
      AUTOTimer.updateTimer();
    }
    updateGyro();
    updateSensorBoard();
    updateIRSensors();
    updateScreen();
  }
}

inline int IRDetermineLocation(int frontS, int backS, int leftS, int rightS) {
  if (varIsAbout(frontS, 230, 30)) {                                  //F=A
    if (varIsAbout(leftS, 43, 10) && varIsAbout(backS, 75, 15))           //if L=I and B=D
      return R4;
    else if (varIsAbout(rightS, 43, 10) && varIsAbout(backS, 65, 15))     //if R=I and B=D
      return L1;
  }
  else if (varIsAbout(frontS, 562, 100) || frontS == 999) {                        //F=B    //
    if (varIsAbout(rightS, 109, 30) && varIsAbout(leftS, 67, 20))          //R==G and L==I  //
      return L2;
    else if (varIsAbout(rightS, 67, 20) && varIsAbout(leftS, 109, 30))     //R==I and L==G
      return R3;
  }
  else if (varIsAbout(frontS, 115, 20)) {                             //F=C
    if (varIsAbout(rightS, 260, 50) && varIsAbout(leftS, 60, 20))         //R==G and L==I
      return L3;
    else if (varIsAbout(leftS, 60, 20) && varIsAbout(leftS, 260, 50))     //R==I and L==G
      return R2;
  }
  else if ( varIsAbout(frontS, 85, 20)) {                            //F=D
    if (varIsAbout(backS, 255, 50) && varIsAbout(leftS, 44, 30))       //R==H and  L==F
      return L4;
    else if (varIsAbout(leftS, 44, 30) && varIsAbout(backS, 255, 50))
      return R1;
  }
  else if (varIsAbout(frontS, 80, 20)) {                              //F=E
    if (varIsAbout(rightS, 250, 50) && varIsAbout(leftS, 60, 30)) {      //R==G L==I
      if (varIsAbout(backS, 250, 55))
        return R5;
      else if (varIsAbout(backS, 532, 200) || backS == 999)
        return R6;
    }
    else if (varIsAbout(rightS, 60, 30) && varIsAbout(leftS, 250, 50)) {  //R==I L==G
      if (varIsAbout(backS, 250, 75) || backS == 999)
        return L5;
      else if (varIsAbout(backS, 532, 200))
        return L6;
    }
  }
}

inline bool doStartTurnAndCenter(int initTurn, bool R_L) {
  continuable = true;
  if (initTurn != 0)  {
    continuable = turn(initTurn);
    betweenMacro();
  }
  if (continuable == true)  {
    continuable = followLeft(leftS, 160);
    betweenMacro();
  }
  if (continuable == true) {
    if (R_L)
      continuable = turn(-90);
    else
      continuable = turn(90);
    betweenMacro();
  }
}
void updateAutonomous(int location)
{
  switch (location) {
    case L1:
      continuable = doStartTurnAndCenter(0, Lside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case L2:
      continuable = doStartTurnAndCenter(60, Lside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case L3:
      continuable = doStartTurnAndCenter(120, Lside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case L4:
      continuable = doStartTurnAndCenter(180, Lside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case L5:
      continuable = doStartTurnAndCenter(-120, Lside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case L6:
      continuable = doStartTurnAndCenter(-60, Lside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case R1:
      continuable = doStartTurnAndCenter(-180, Rside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case R2:
      continuable = doStartTurnAndCenter(-120, Rside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case R3:
      continuable = doStartTurnAndCenter(-60, Rside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case R4:
      continuable = doStartTurnAndCenter(0, Rside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case R5:
      continuable = doStartTurnAndCenter(60, Rside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case R6:
      continuable = doStartTurnAndCenter(120, Rside);
      while (continuable)
        continuable = straightShot(continuable);
      break;
    case 13:
      continuable = true;
      do
      {
        continuable = straightShot2(continuable);
      }
      while (continuable);
      break;
      //      continuable = true;
      //      do
      //      {
      //        continuable = followLeft(leftS, 120);
      //        betweenMacro();
      //        if (continuable == true)
      //        {
      //          continuable = turn(-90);
      //          betweenMacro();
      //        }
      //      }
      //      while (continuable);
      //      break;
    case 14:
      continuable = true;
      do
      {
        continuable = straightShot2(continuable);
      }
      while (continuable);
      break;
    case 15:
      continuable = true;
      do
      {
        continuable = straightShot(continuable);
      }
      while (continuable);
      break;
    case 16:
      continuable = true;
      do
      {
        continuable = straightShot(continuable);
      }
      while (continuable);
      break;
    case 17:
      continuable = true;
      do
      {
        continuable = straightShot(continuable);
      }
      while (continuable);
      break;

  }
}
//************************************AUTONOMOUS HANDLER FOR COMMANDS************************************
void updateAutonomous()
{
  if (autoSet == 1)
  {
    //Assume DEFCON 3 (Nuclear Tactical Avoidance Systems)
    initMacroSystem(AUTO);

    //Standard Autonomous mode to drive course regularly
    //Template in pieces.h
    switch (autoState)
    {
      case 0:
        updateAutonomous(IRDetermineLocation(frontS, backS, leftS, rightS));
        break;

      default:
        updateAutonomous(autoState);
        break;
    }
    autoSet = 0;
    autoState = 0;
  }
}

//***************************************************************************************
//************************************ MACRO SYSTEMS ************************************
//***************************************************************************************
//---METHOD TO START A MACRO SYSTEM COMMAND------
inline void initMacroSystem(int state) {
  setColor(1570, 0, 1600);
  betweenMacro();
  delay(5);
  //Tell the control box we have entered the macro system (send to control box)
  //will get resent as data gets updated with method screen update
  toControl.macroCommand = macroRequest;
  to_control.sendData();
  delay(5);
  //Show Everyone We Are In Autonomous Mode (On board)
  currentState = state;
  updateLeds();

  //Motor Preparation (Send all stop to motor board)
  allStop();
  motor_unStick();

  //Sensor Preparation
  //Set gyro to 0' before we use it at all
  zeroInternalAngle();
  actuatorUpdate();
}

//--------METHOD TO FINISH A MACRO SYSTEM COMMAND-------------
inline void finishMacro()
{
  setColor(1570, 0, 1600);
  //Tell the control box that macro complete (send to control)
  toControl.macroCommand = 0;
  toControl.macro_complete = 1;
  delay(10);
  to_control.sendData();
  sender = 0;
  while (macroRequest != 0)  {
    if (from_control.receiveData())
      macroRequest = fromControl.macro_command;

    sender += 1;
    delay(50);
    if (sender >= 10)
    {
      to_control.sendData();
      sender = 0;
    }
  }
  //Sensor Refresh/Clear
  zeroInternalAngle();
  angleSet = 0;
  firstShot = true;
  delay(25);
  commTimer.resetTimer();
  //Return the Robot to drive mode
  currentState = 1;
  macroRequest = 0;
  toControl.macro_complete = 0;
}

//-----------------MACRO LOOPS----------------------
// The following functions are ones used during macros
// most feature a while loops that sustains comms and runs macros
//********************INFRARED MACRO METHODS*************************
inline bool followRight(int target, int rangeFront) {
  continueMacro = 0;
  int magnitude;
  int outputSide;
  int travelDistanceRemaining = frontS - rangeFront;

  PID sideError(target, sideKpR, sideKiR, sideKdR, 0);
  sideError.clearSystem();
  PIDTimer.resetTimer();
  while ((frontS >= rangeFront) && continueMacro == 0)  {
    updateGyro();
    updateSensorBoard();                //Get new data in from sensor board
    updateIRSensors();                     //update IR equations for new values

    PIDTimer.updateTimer();
    if (PIDTimer.timerDone()) {
      outputSide = sideError.updateOutput(rightS);                  //calculate command for turning
      travelDistanceRemaining = ( frontS - rangeFront );

      if (travelDistanceRemaining > slowingDistance)
        magnitude = motorHigh;
      else
        magnitude = motorHigh - (travelDistanceRemaining / slowingDistance) * motorHigh; //calculate speed forward

      if (!(varIsAbout(target, rightS, sideDeadR)) && abs(macroAngle < gyroCorrectR))    //if you have skewd off course
        differentialDrive(magnitude, outputSide);                                         //correct if gyro doesnt say we are over correcting
      else if (abs(macroAngle) > gyroCorrectR)
        turn(-macroAngle);
      else     //else turn back
        variableDrive(constrain(magnitude, motorLow, motorHigh));

      //send motor data
      to_motor.sendData();
      if (from_control.receiveData()) {
        //report to the screen with pid data
        updateScreen(sideError);
        //check for command halt
        continueMacro = fromControl.macro_stop;
      }
    }
  }
  //end command
  allStop();
  motor_unStick();

  //return if halt
  return (continueMacro == 0);
}
//-----------------------------------------------------------------------------------
inline bool followLeft(int rangeSide, int rangeFront)
{
  //record the initial sensor used to perform target management

  continueMacro = 0;
  int magnitude;
  int outputSide;
  int travelDistanceRemaining = frontS - rangeFront;
  PID sideError(rangeSide, sideKpL, sideKiL, sideKdL, 0);
  sideError.clearSystem();
  PIDTimer.resetTimer();
  while ((frontS >= rangeFront) && continueMacro == 0)  {
    //update all readings before we make decisions (some are throttled in the background with timers)
    updateGyro();
    updateSensorBoard();
    updateIRSensors();


    //update the PID system timer
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())  //Check if the timer is done
    {
      //calculate the amount of travel forward left and set up speed control
      magnitude = constrain((frontS - rangeFront), motorLow, motorHigh);
      outputSide = (sideError.updateOutput(leftS)); //if timer is done make a PID decision

      if (!(varIsAbout(rangeSide, leftS, sideDeadL)) && abs(macroAngle < gyroCorrectL))
        differentialDrive(magnitude, outputSide);
      else if (abs(macroAngle > gyroCorrectL))
        differentialDrive(magnitude, -outputSide);
      else
      {
        variableDrive(magnitude);
        sideError.clearIntegral();
      }
      //send motor data
      to_motor.sendData();
      if (from_control.receiveData()) {
        //report to the screen
        updateScreen(sideError);
        //check for halt command
        continueMacro = fromControl.macro_stop;
      }
    }
  }
  //end command
  allStop();
  motor_unStick();

  //return if halt
  return (continueMacro == 0);
}


//-----------------------------------------------------------------------------------
inline bool driveUntilIRFront (int distance) {
  continueMacro = 0;
  int magnitude;
  PID driveForward(distance, 1, 0, 0, 1);
  driveForward.clearSystem();
  PIDTimer.resetTimer();
  while ((frontS >= distance) && continueMacro == 0)
  {
    updateSensorBoard();
    updateIRSensors();
    //update the PID system timer
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())  //Check if the timer is done
    {

      magnitude = driveForward.updateOutput(frontS); //choose how fast to command

      variableDrive(constrain(magnitude, motorLow, (motorHigh))); //constrain and command

      to_motor.sendData();  //send motor data

      if (from_control.receiveData()) {
        updateScreen(driveForward);        //report to the screen
        continueMacro = fromControl.macro_stop;  //check for command halt
      }
    }

  }
  //end command
  allStop();
  motor_unStick();

  //return if halt
  return (continueMacro == 0);
}



//******************GYROSCOPE TURNING MACRO******************
inline bool turn(int setAngle)
{
  int magnitude = 0;
  numberRuns += 1;
  if (numberRuns > 5)
  {
    setColor(YELLOW);
    calibrateZero(200);
    setColor(GREEN);
    numberRuns = 0;
  }
  //internalize the number of degrees to turn

  angleSet = setAngle;

  //set variable to listen to control board
  continueMacro = 0;
  PID output(setAngle, gyroKp, gyroKi, gyroKd, 2);
  output.clearSystem();
  PIDTimer.resetTimer();
  //while the robot is not0 facing the angle requested

  //for (int i = 0; i < 2; i++) {
  while (!(macroAngle < angleSet + 1 && macroAngle > angleSet - 1) && (continueMacro == 0))
  {

    //update our internal angle
    updateGyro();
    //update the PID system timer
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())  //Check if the timer is done
    {
      //update decision making
      turnHelp(output.updateOutput(macroAngle));
      //send motor data to motor board
      to_motor.sendData();
      PIDTimer.resetTimer();
      //check for stop command from control
      if (from_control.receiveData())
      {
        //send updates to screen
        updateScreen(output);
        continueMacro = fromControl.macro_stop;

      }
    }
  }
  //    allStop();
  //    motor_unStick();
  // delay(50);
  //}
  //freeze motors after complete
  allStop();
  motor_unStick();
  //zero macro angle
  zeroInternalAngle();

  //END TURNING
  return (continueMacro == 0);
}

//******************FUTURE ENCODER MACROS******************
//current drive for a duration of time in millis
//sign makes a different direction
inline bool drive(int length)
{
  continueMacro = 0;
  int duration = abs(length);        //grab the magnitude of the drive
  //(the sign says direction)
  int timeNow = millis();        //create time keeping variables
  int timeFinish = timeNow + (duration * 125);
  while ((timeNow < timeFinish) && (continueMacro == 0))
  {

    if (length < 0)
    {
      variableDrive(-12);
    }
    else if (length > 0)
    {
      variableDrive(12);
    }
    to_motor.sendData();
    if (from_control.receiveData())
    {
      continueMacro = fromControl.macro_stop;
      //Update Screen output on control board
      updateScreen();
    }

    timeNow = millis();
  }
  //stop the robot after
  allStop();
  motor_unStick();
  return (continueMacro == 0);
}

//-----------------------------------------------------------------------------------
//set the actuator to the angle specified
inline bool actuator(int target)
{
  continueMacro = 0;
  int magnitude;
  //while the actuator is not at the target(within 1)
  while (!(actuatorAngle >= target - 1 && actuatorAngle <= target + 1) && continueMacro == 0)
  {
    //update the value of the actuator
    actuatorUpdate();
    //choose how fast to command
    magnitude = (target - actuatorAngle) * 5;
    //set actuator movement to that speed(signed direction)
    actuatorAdjust(magnitude);
    //send motor data
    to_motor.sendData();


    //check for command halt
    if (from_control.receiveData())
    {
      //report to the screen
      updateScreen();
      continueMacro = fromControl.macro_stop;
    }
  }
  //end command
  allStop();
  motor_unStick();

  //return if halt
  return (continueMacro == 0);
}

//-----------------------------------------------------------------------------------
//raise bucket to near max and then drop it to driving position
inline bool actuatorDump()
{
  //set continue variable to 1
  continueMacro = 0;

  //raise the actuator
  actuator(97);
  digitalWrite(VIBRATOR, LOW);
  PIDTimer.resetTimer();
  //create time keeping variables
  int timeNow = millis();
  int timeFinish = timeNow + 1200;
  //do a wait and update, while time isnt set
  while ((timeNow < timeFinish) && (continueMacro == 0))
  {
    PIDTimer.updateTimer();
    if (PIDTimer.timerDone())
    {
      if (from_control.receiveData())
      {
        updateScreen();
        continueMacro = fromControl.macro_stop;
      }
    }
    timeNow = millis();

  }
  digitalWrite(VIBRATOR, HIGH);
  //after the wait lower the actuator
  actuator(actuatorDrivingAngle);
  return (continueMacro == 0);
}



inline bool dDrive(int offset)
{
  continueMacro = 0;

  //grab the magnitude of the drive
  //(the sign says direction)
  int duration = 6;

  //create time keeping variables
  int timeNow = millis();
  int timeFinish = timeNow + (duration * 200);

  //drive while time not reached
  while ((timeNow < timeFinish) && (continueMacro == 0))
  {
    if (from_control.receiveData())
    {
      updateScreen();
      continueMacro = fromControl.macro_stop;
    }
    differentialDrive(20, -offset * 1.5);
    to_motor.sendData();
    timeNow = millis();
  }
  //stop the robot after
  allStop();
  motor_unStick();
}


//******************LOW LEVEL ROBOT METHODS (USED INTERNALLY MOSTLY)*****************
//Functions to Manipulate the Robot
//_____________________________________________________________________
//Robot motors ALL Stop
inline void allStop() {
  outMotor.leftMotorSpeed  = 255;
  outMotor.rightMotorSpeed = 255;
  outMotor.actuator = 255;
  to_motor.sendData();
}

//When motor board recieves 255's it appears to lock up,
// this method assures they are unstuck (as one may assume :) )
inline void motor_unStick() {
  outMotor.leftMotorSpeed  = 0;
  outMotor.rightMotorSpeed = 0;
  outMotor.actuator = 0;
  to_motor.sendData();
}
//-----------------------------------------------------------------------------------
//driving helper method, allows for commanding just the motors
//to a magnitude that matches, sign determines direction
inline void variableDrive(int mag) {
  outMotor.leftMotorSpeed = mag;
  outMotor.rightMotorSpeed = mag;
}
//------------------------------------------------------------------
//actuator help method  -- send a speed of adjustment
inline void actuatorAdjust(int mag) {
  //limit commanded speed to range -90->90
  if (abs(mag) > 90)  {
    if (mag < 0)   mag = -90;
    if (mag > 0)   mag =  90;
  }
  //make sure the movement isnt too slow
  if (abs(mag) < 50)  {
    if (mag < 0)   mag = -50;
    if (mag > 0)   mag =  50;
  }
  outMotor.actuator = mag;
}

//-----Scootch (just to wiggle our IRs to possibly grab unknown stuff----
inline void scootch() {
  differentialDrive(4, 2);
  to_motor.sendData();
  delay(2);
  differentialDrive(4, -2);
  to_motor.sendData();
  delay(2);
}

//-----------------turning helper method.-----------------------
inline void turnHelp(int mag) {
  if (mag > 0)
    mag = constrain(mag, motorLow, motorHigh);
  else
    mag = constrain(mag, -motorHigh, -motorLow);

  outMotor.leftMotorSpeed  = -mag;
  outMotor.rightMotorSpeed = mag;
}

//-----METHOD TO SEND INFORMATION TO CONTROL BOX------
//Display update during the macro/autonomous commands
inline void updateScreen(PID runningPID)
{
  screenTimer.updateTimer();
  if (screenTimer.timerDone())
  {
    //Actuator gyro
    toControl.bucketAngle = actuatorAngle;
    toControl.gyroAngle = macroAngle;
    //long IR
    //    toControl.IRRightLong = IRLR;
    //    toControl.IRLeftLong = IRLL;
    //    toControl.IRBackLong = IRLB;
    //    toControl.IRFrontLong = IRLF;
    //    //MID IRs
    //    toControl.IRBackMid = IRMB;
    //    toControl.IRLeftMid = IRML;
    //    toControl.IRRightMid = IRMR;
    //    //BACK IRs
    //    toControl.IRBackL = IRSBL;
    //    toControl.IRBackR = IRSBR;
    //    //FRONT IRs
    //    toControl.IRFrontR = IRSFR;
    //    toControl.IRFrontL = IRSFL;
    //simplified IR
    toControl.F = frontS;
    toControl.L = leftS;
    toControl.R = rightS;
    toControl.B = backS;
    //motor speeds
    toControl.motorL = outMotor.leftMotorSpeed;
    toControl.motorR = outMotor.rightMotorSpeed;
    toControl.PIDerror = runningPID.readError();
    toControl.PIDintegral = runningPID.readIntegral();
    toControl.PIDderivative = runningPID.readDerivative() * 10000;
    toControl.PIDoutput = runningPID.readOutput();
    toControl.PIDnumber = runningPID.readNumber();

    to_control.sendData();
  }
}
inline void updateScreen()
{
  screenTimer.updateTimer();
  if (screenTimer.timerDone())
  {
    //Actuator gyro
    toControl.bucketAngle = actuatorAngle;
    toControl.gyroAngle = macroAngle;
    //long IR
    //    toControl.IRRightLong = IRLR;
    //    toControl.IRLeftLong = IRLL;
    //    toControl.IRBackLong = IRLB;
    //    toControl.IRFrontLong = IRLF;
    //    //MID IRs
    //    toControl.IRBackMid = IRMB;
    //    toControl.IRLeftMid = IRML;
    //    toControl.IRRightMid = IRMR;
    //    //BACK IRs
    //    toControl.IRBackL = IRSBL;
    //    toControl.IRBackR = IRSBR;
    //    //FRONT IRs
    //    toControl.IRFrontR = IRSFR;
    //    toControl.IRFrontL = IRSFL;
    //simplified IR
    toControl.F = frontS;
    toControl.L = leftS;
    toControl.R = rightS;
    toControl.B = backS;
    //motor speeds
    toControl.motorL = outMotor.leftMotorSpeed;
    toControl.motorR = outMotor.rightMotorSpeed;

    to_control.sendData();
  }
}


















