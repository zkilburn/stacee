Adafruit_Thermal printer(36, 33);

Timers LEDTimer(1000);
Timers sendTimer(50);
Timers screenTimer(15);
Timers commTimer(1000);
Timers latency(50);

bool readyToRead=false, readyToSend=true;
//-----------for constants send screen-------------------------------
bool variableChosen=false;
bool valueChosen=false;
int variableRequest, valueRequest;
int thousands, hundreds, tens,ones,stored;
int lastWrite=0;
int largeOnes=0, largeTens=0, tenToThe=0;
bool large=false, storeLarge=false, negative=false;
int valueDispInt, valueDispExp, workingRegDisp;
bool valueDispNeg=false;
//------------------helpful variable---------------------------
int plyable;
int handy = 0;
bool booly = false;
//----------------Tuning Variables--------------------------
int gyroConLow = 15;
int gyroConHigh = 40;
int motorConLow = 8;
int motorConHigh = 18;
//------------------------------------------------------------------------
EasyTransferCRC communicationBoardOut; //for sending commands to the robot
EasyTransferCRC controlET;  //for receiving data back from the robot
//------------------------------------------------------------------------
//-----------------internalized vars of robot data------------------------
//------------------------------------------------------------------------
int gyroAngle = 999;
int bucketAngle = 999;
int errorMargin = 999;
int robotHeading = 0;
int outputHeading = 0;
float totalTime=0;
//IR Selected
int F;
int L;
int R;
int B;
int volts = 260;
int amps = 20, watts;
int debug1;
int debug2;
int debug3;
int debug4;
//IR VALUES UPDATES FOR SCREEN
int IRFrontLong = 550;
int IRBackLong = 550;
int IRLeftLong = 550;
int IRRightLong = 550;
int IRRightMid = 80;
int IRFrontL = 150;
int IRFrontR = 150;
int IRLeftMid = 80;
int IRBackMid = 80;
int IRBackL = 30;
int IRBackR = 30;
int IRFrontLongR = 550;
int IRBackLongR = 550;
int IRLeftLongR = 550;
int IRRightLongR = 550;
int IRRightMidR = 80;
int IRFrontLR = 150;
int IRFrontRR = 150;
int IRLeftMidR = 80;
int IRBackMidR = 80;
int IRBackLR = 30;
int IRBackRR = 30;
//ACTUATOR DATA UPDATES
int actuatorData = 30;
//VARIABLE TO CATCH REQUESTS FROM SCREEN
int diff_turn = 0;       //L/R DIFF
int driveDistance = 0;   //F/R DIFF
//HEADING CALCULATION FROM GYROSCOPE PAGE
int calcHeading = 0;
int angleOffset = 0;
//command angle for gyroscope knob
int angle_command = 0;
//----------------------------------------------------------------------------
//in development profile variable for changing driving functionality
int drivingProfile = 1;
//State Control Variables
int leftMotorSpeed = 0;
int rightMotorSpeed = 0;
int actuatorSpeed = 0;
int stateControl = 1;
//comboard macroback
int commMacro = 0;
int macro_complete = 0;
//macro keeper here on control
int internalMacroKeeper;
int internalSubKeeper;
//KEEPER OF WHICH PAGE THE SCREEN IS READING
int activePage = 0;
int pageKeeper = 0;
//screen macro controls
int macro_command_screen = 0;
int macro_sub_command = 0;

//checksum for screen comm
int checksum = 0;
//counter for ensured data packet flow
int sender = 0;
//package for screen comm packet
byte receivedPack[6];

