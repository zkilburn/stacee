//comms.h

void initializeCommunicaton()
{
  //screen init
  Serial.begin(115200);
  //robot init
  Serial1.begin(115200);
  controlET.begin(details(toComm), &Serial1);
  communicationBoardOut.begin(details(commandBack), &Serial1);
  toComm.state = stateControl;  
}
inline void packetWait()
{
  float time=millis();
  float lastTime=time;
  pageKeeper = activePage;  //record which page is active (to return to after macro)
  writeObject(FORMMESSAGE, COMMALERT, 0);    //change the page to the active macro page 
  sendTimer.resetTimer(); 
  while (!communicationBoardOut.receiveData())
  {
    lastTime=time;
    time=millis();
    totalTime+=((time-lastTime)/1000);
    sendTimer.updateTimer();
    if (sendTimer.timerDone())
    {
      toComm.leftMotorSpeed = leftMotorSpeed;
      toComm.rightMotorSpeed = rightMotorSpeed;
      toComm.actuator = actuatorSpeed;
      toComm.state = stateControl;
      toComm.macro_command = internalMacroKeeper;
      controlET.sendData();

    }
    if (digitalReadFast(MACRO_BUTTON) == HIGH)
    {
      killMacro();
    }
    writeObject(LEDDIGITS, 3, totalTime);    //


  }   

  readyToSend=true;
  sendTimer.resetTimer(); 
  commTimer.resetTimer();
  writeObject(FORMMESSAGE, pageKeeper, 0);    //change the page  
}

inline void commSafety()
{
  commTimer.updateTimer();
  if (commTimer.timerDone())
  {
    packetWait();
    commTimer.resetTimer();
  }  

}
inline void updateCommunication()
{
  //  latency.updateTimer();
  //  if(latency.timerDone()&&readyToSend)
  //  {
  //    controlET.sendData();
  //   readyToSend=false; 
  //   sendTimer.resetTimer();
  //  }

  sendTimer.updateTimer();
  if (sendTimer.timerDone()) //&& !readyToSend)
  {
    controlET.sendData();
    sendTimer.resetTimer();    


    //control box is always listening to the robot
    //in order to keep track of macro's and keep
    //screen up to date.
    if (communicationBoardOut.receiveData())
    {
      //if good checksum, store data locally.
      commMacro = commandBack.macro_command;
      gyroAngle = commandBack.gyroAngle;
      bucketAngle = commandBack.bucketAngle;
      //errorMargin = commandBack.errorMargin;
      robotHeading = commandBack.robotHeading;
      //Front IR Recieve
      //    IRFrontLong = commandBack.IRFrontLong;
      //    IRBackLong = commandBack.IRBackLong;
      //    IRLeftLong = commandBack.IRLeftLong;
      //    IRRightLong = commandBack.IRRightLong;
      //    //Mid IR Recieve
      //    IRRightMid = commandBack.IRRightMid;
      //    IRLeftMid = commandBack.IRLeftMid;
      //    IRBackMid = commandBack.IRBackMid;
      //    //Back IR Recieve
      //    IRBackL = commandBack.IRBackL;
      //    IRBackR = commandBack.IRBackR;
      //    //Front IR Recieve
      //    IRFrontL = commandBack.IRFrontL;
      //    IRFrontR = commandBack.IRFrontR;
      //
      //    //debug variables
      //    debug1 = commandBack.debug1;
      //    debug2 = commandBack.debug2;
      //    debug3 = commandBack.debug3;
      //    debug4 = commandBack.debug4;

      //Power systems
      volts = commandBack.volts;
      amps = commandBack.amps;
      watts = commandBack.watts;

      //Selected Values Select
      F = commandBack.F;
      L = commandBack.L;
      R = commandBack.R;
      B = commandBack.B;

      ////RAW IR
      ////LONG IR Recieve
      //IRFrontLongR = commandBack.IRFrontLongR;
      //IRBackLongR = commandBack.IRBackLongR;
      //IRLeftLongR = commandBack.IRLeftLongR;
      //IRRightLongR = commandBack.IRRightLongR;
      ////Mid IR Recieve
      //IRRightMidR = commandBack.IRRightMidR;
      //IRLeftMidR = commandBack.IRLeftMidR;
      //IRBackMidR = commandBack.IRBackMidR;
      ////Back IR Recieve
      //IRBackLR = commandBack.IRBackLR;
      //IRBackRR = commandBack.IRBackRR;
      ////Front IR Recieve
      //IRFrontLR = commandBack.IRFrontLR;
      //IRFrontRR = commandBack.IRFrontRR;

      //only store the value of the complete macro
      //if we are looking for it
      if (internalMacroKeeper != 0)
        macro_complete = commandBack.macro_complete;
      else
        macro_complete = 0;


      toComm.leftMotorSpeed = leftMotorSpeed;
      toComm.rightMotorSpeed = rightMotorSpeed;
      toComm.actuator = actuatorSpeed;
      toComm.state = stateControl;
      toComm.macro_command = internalMacroKeeper;


      commTimer.resetTimer();    
      readyToSend=true;  
      latency.resetTimer();  
      sendTimer.resetTimer();
    }

    else if(internalMacroKeeper==0)
      commSafety();
  }
}






