//screen.h
//Zac Kilburn
//3-8-14

//Screen.h handles all of the screens input and output systems.
//finally have an extremely stable version of this code for comms
//
//the protocol is as follows
//byte #         0             1            2       3    4    5
//           [command #] [typeOfEntity] [Entity#] [dH]  [dL] [CRC]
// byte 0 ---- 0x07 is a command output from screen
// byte 1 ---- entity type is a button or slider etc.
// byte 2 ---- which button slider etc was pressed
// byte 3/4 -- the data values involved (used for sliders not buttons)
// byte 5 ---- cyclic redundancy check (simple xor with each byte)

//Revised this to only send the screen the data it needs for the page it is preseenting
inline static bool readStream()
{
  if (Serial.available() > 5)
  {
    //while bytes are still there
    while (Serial.available())
    {
      //the the leading byte is not a 0x07 (what we need)
      if (Serial.peek() != 0x07)
      {
        //eat it out and check again
        Serial.read();
      }
      else
      {
        //else get outta here (its a 0x07)
        break;
      }
    }
    //if there are at least our 6 byte packet in the stream
    if (Serial.available() > 5)
    {
      // get the all 6 of the bytes in the packet
      receivedPack[0] = Serial.read();
      //calculating the checksum as we go
      checksum  = receivedPack[0];
      receivedPack[1] = Serial.read();
      checksum ^= receivedPack[1];
      receivedPack[2] = Serial.read();
      checksum ^= receivedPack[2];
      receivedPack[3] = Serial.read();
      checksum ^= receivedPack[3];
      receivedPack[4] = Serial.read();
      checksum ^= receivedPack[4];
      receivedPack[5] = Serial.read();
      checksum ^= receivedPack[5];
      return true;
    }
  }
  else
    return false;
}
inline static bool writeContrast(int value){  
  Serial.write(CONTRAST);
  checksum = CONTRAST;
  delayMicroseconds(100);
  Serial.write((int8_t)value);
  checksum^= value;
  delayMicroseconds(100);
  Serial.write(checksum); 
 
 while (!Serial.available()&& !((Serial.peek()==0x06) || (Serial.peek()==0x15)))
  {
    if(Serial.available()>5 && Serial.peek()==0x07)
    {
      readObject();
      return false;
    }    
  }
  if (Serial.peek() == 0x06)
  {
    Serial.read();
    return true;
  }
  else if (Serial.peek() == 0x15)
  {
    Serial.read();
    return false;
  }
  else
    return false; 
}
//---------------------------------------------------------------------
inline static bool writeObject(byte object, byte index, unsigned int data)
{
  byte lsb = (data >> 0) & 0xFF;
  byte msb = (data >> 8) & 0xFF;
  byte checksum = 0;

  Serial.write(0x01);
  checksum  = 0x01;
  delayMicroseconds(100);
  Serial.write(object);
  checksum ^= object;
  delayMicroseconds(100);
  Serial.write(index);
  checksum ^= index;
  delayMicroseconds(100);
  Serial.write(msb);
  checksum ^= msb;
  delayMicroseconds(100);
  Serial.write(lsb);
  checksum ^= lsb;
  delayMicroseconds(100);
  Serial.write(checksum); 
  delayMicroseconds(100); 
  while (!Serial.available()&& !((Serial.peek()==0x06) || (Serial.peek()==0x15)))
  {
    if(Serial.available()>5 && Serial.peek()==0x07)
    {
      readObject();
      return false;
    }    
  }
  if (Serial.peek() == 0x06)
  {
    Serial.read();
    return true;
  }
  else if (Serial.peek() == 0x15)
  {
    Serial.read();
    return false;
  }
  else
    return false;

}
//---------------------------------------------------------------------
//this function updates the screen (externally called)
void updateScreen()
{
  readObject();
  writeScreen();
}
//---------------------------------------------------------------------
//Check for serial available--
//if packet is present, parse it and instruct robot accordingly
inline static bool readObject()
{
  //Erase the old pack before parsing
    for (int i = 0; i < 6; i++)
    {
      receivedPack[i] = 0;
    }
  readStream();
  //check to see if a packet persists in memory
  // Compare byte 1 to check for CMD Message
  if (receivedPack[0] == 0x07)
  {
    //store the data as a word (not a byte)
    unsigned int dataIn = (receivedPack[3] << 8) + receivedPack[4];
    switch (receivedPack[1])
    {
        //if byte 2 is a button
      case BUTTON:
        //check to see which button
        switch (receivedPack[2])
        {
            //--------------TURNN PAGE------------------------------------------
            //Right 45
          case 22: //22
            macro_command_screen = TURN;
            macro_sub_command = -45;
            break;
            //right 90
          case 24: //24
            macro_command_screen = TURN;
            macro_sub_command = -90;
            break;
            //right 135
          case 27: //27
            macro_command_screen = TURN;
            macro_sub_command = -135;
            break;
            //turn around
          case 25: //25
            macro_command_screen = TURN;
            macro_sub_command = 180;
            break;
            //left 45
          case 21: //21
            macro_command_screen = TURN;
            macro_sub_command = 45;
            break;
            //left 90
          case 26:  //26
            macro_command_screen = TURN;
            macro_sub_command = 90;
            break;
            //left 135
          case 23: //23
            macro_command_screen = TURN;
            macro_sub_command = 135;
            break;
            //-------------------Autonomous Commands-------------------------------------
            //Autonomous Mode
          case 18:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = autoMode0;
            break;
          case 7:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = autoMode1;
            break;
          case 9:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = autoMode2;
            break;
          case 8:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = autoMode3;
            break;
          case 16:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = autoMode4;
            break;
          case L1:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 1;
            break;
          case L2:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 2;
            break;
          case L3:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 3;
            break;
          case L4:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 4;
            break;
          case L5:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 5;
            break;
          case L6:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 6;
            break;
          case R1:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 7;
            break;
          case R2:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 8;
            break;
          case R3:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 9;
            break;
          case R4:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 10;
            break;
          case R5:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 11;
            break;
          case R6:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = 12;
            break;
          case GOLDENAUTOBUTTON:
            macro_command_screen = autonomousFeatures;
            macro_sub_command = fullAuto;
          default:
            break;
            //--------------------Actuator Page------------------------------------
            //setActuator
          case 0x1E:
            macro_command_screen = ACTUATOR;
            macro_sub_command = actuatorData;
            break;
            //dump
          case 0x1F:
            macro_command_screen = ACTUATOR;
            macro_sub_command = DUMP;
            break;
            //mine
          case 0x20:
            macro_command_screen = ACTUATOR;
            macro_sub_command = MINE;
            break;

            //----------------------Battery Information Page-----------------------
            //Printer Button
          case 14:
            printerPrint();
            break;
            //-------------------------IR Follow----------------------
          case  FOLLOWRIGHT:
            macro_command_screen = 7;
            macro_sub_command = 30;
            break;
          case  FOLLOWLEFT:
            macro_command_screen = 8;
            macro_sub_command = 30;
            break;
          case INITIR:
            macro_command_screen = additionalFeatures;
            macro_sub_command = unstickIRs;
            break;
            //--------------------MACRO ACTIVE PAGE BUTTONS----------------------
          case CANCELMACRO:
            killMacro();
            //------------------------------Variable Sender Buttons-----------------
          case COMMITTOSEND:
            macro_command_screen = constants + variableRequest; //(70+variable requested)
            macro_sub_command = valueRequest;
            break;
          case WIPEREGS:
            variableRequest = 0, valueRequest = 0;
            thousands = 0, hundreds = 0, tens = 0, ones = 0, stored = 0,  workingRegDisp=0;
            valueDispInt = 0, valueDispExp = 0;
            valueDispNeg = false;
            largeOnes = 0, largeTens = 0, tenToThe = 0, large = false, storeLarge = false;
            variableChosen = false, valueChosen = false, negative = false;
            break;

        }
        break;
        //slider update
      case SLIDER:
        //check to see which slider
        switch (receivedPack[2])
        {
          case 0:
            actuatorData = dataIn;
            break;
          case 1:
            driveDistance = dataIn;
            break;
          case 2:
            angle_command = dataIn;
            break;
          case 3:
            diff_turn = dataIn;
            break;
          case gyroLow:
            gyroConLow = dataIn;
            break;
          case gyroHigh:
            gyroConHigh = dataIn;
            break;
          case motorLow:
            motorConLow = dataIn;
            break;
          case motorHigh:
            motorConHigh = dataIn;
            break;
        }
        break;
        //received is a knob
      case KNOB:
        break;
        //else update the form that is currently up
      case FORMMESSAGE:
        activePage = receivedPack[2];
        switch(activePage)
        {
          case CONSTANTSEND:
           screenTimer.setInterval(150); 
           break;
           case PID:
           screenTimer.setInterval(25);
           break;
           case MACROACTIVE:
           screenTimer.setInterval(25);
           break;
          default:
          screenTimer.setInterval(15); 
           break; 
      }
        break;
      case KEYBOARD:
      //--------------------------BACK KEY PRESSED------------------------------------
        if (dataIn == BACK) 
        {
          if (stored != 0) //first wipe the value in the working number registers
          {
            thousands = 0,
            hundreds = 0,
            tens = 0,
            ones = 0,
            stored = 0,
            largeOnes = 0,
            largeTens = 0,
            tenToThe = 0,
            workingRegDisp=0;            
            large = false,
            storeLarge = false;
            negative = false;
          }
          else if (variableChosen && !valueChosen)  //if you already wiped the regs, and you have a var but not a value
          {
            variableRequest = 0;      //wipe the var
            variableChosen = false;
            negative = false;
             writeObject(STRINGS , 1 , 0);    //update the screen to the number requested.
          }
          else if (valueChosen) //else wipe the value
          {
            stored = 0,
            largeOnes = 0,
            largeTens = 0,
            tenToThe = 0;            
            valueDispInt = 0, valueDispExp = 0;
            valueDispNeg = false;
            valueRequest = 0;
            valueChosen = false;
          }
        }
        //--------------------------ENTER KEY PRESSED------------------------------------
        else if (dataIn == ENTER) // received an enter command
        {
          if (storeLarge)             //if we are storing a large variable (x10^) field disabled
          {
            storeLarge = !storeLarge;
            large = true;              //let the system know to calculate any variables for sci representation
          }
          if (!variableChosen)     //if we are going to store the latest value as a variable
          {
            variableRequest = constrain(stored,0,16);   //store it
            stored = 0;                  //and wipe the rest away
            thousands = 0;
            hundreds = 0;
            tens = 0;
            ones = 0;
            workingRegDisp=0;
            variableChosen = true;                  //record the fact we have one stored            
            
          }
          else if (!valueChosen)
          {
            valueRequest = stored;
            valueDispInt = thousands + hundreds + tens + ones; //prepare values to display before wiping the working register
            valueDispExp = largeTens + largeOnes;        //^^
            valueDispNeg = negative;                      //^^
            stored = 0;                  //wipe it all out
            thousands = 0;
            hundreds = 0;
            tens = 0;
            ones = 0;
            workingRegDisp=0;
            tenToThe=0;
            largeTens=0;
            largeOnes=0;
            valueChosen = true;
          }
        }
        //--------------------------NEGATE KEY PRESSED------------------------------------
        else if (dataIn == '+') //negate command
        {
          if (variableChosen)   //only negate if the value is not intended to be used as a variable
          {
            if (negative)     //activate the negative system.
            {
              negative = false;
              stored *= (-1);
            }
            else
            {
              negative = true;
              stored *= (-1);
            }
          }
        }
        //--------------------------DELETE KEY PRESSED------------------------------------
        else if (dataIn == 'D') //delete command (working register)
        {
          if (storeLarge)
          {
            largeOnes = largeTens / 10;
            largeTens = 0;
            tenToThe = largeOnes + largeTens;
          }
          else
          {
            ones = tens / 10;
            tens = hundreds / 10;
            hundreds = thousands / 10;
            thousands = 0;
            if (negative)
              stored = -(thousands + hundreds + tens + ones);
            else
              stored = thousands + hundreds + tens + ones;
            if (large)
              for (int i = 0; i < tenToThe; i++)
                stored *= 10;
          }
        }
        //--------------------------CLR KEY PRESSED------------------------------------
        else if (dataIn == 'C') //clear command to wipe all regs
        {
          valueDispInt = 0, valueDispExp = 0;
          valueDispNeg = false;
          variableRequest = 0, valueRequest = 0;
          thousands = 0, hundreds = 0, tens = 0, ones = 0, stored = 0,workingRegDisp=0;
          largeOnes = 0, largeTens = 0, tenToThe = 0, large = false;
          variableChosen = false, valueChosen = false;
          negative = false;
        }
        //--------------------------EXP KEY PRESSED------------------------------------
        else if (dataIn == 'x') // to do large numbers
        {
          if (variableChosen)
            storeLarge = !storeLarge; //toggle the storing system to work with the large number system addon
        }
        //--------------------------NUMBER KEY PRESSED------------------------------------
        else
        {
          if (storeLarge)
          {
            largeTens = largeOnes * 10;
            largeOnes = dataIn - 48;
            tenToThe = largeTens + largeOnes;
            large = true;
          }
          else
          {
            thousands = hundreds * 10;
            hundreds = tens * 10;
            tens = ones * 10;
            ones = dataIn - 48;
            workingRegDisp=ones+tens+hundreds+thousands;
            if (negative)
              stored = -(thousands + hundreds + tens + ones);
            else
              stored = thousands + hundreds + tens + ones;
            if (large)
              for (int i = 0; i < tenToThe; i++)
                stored *= 10;
          }
        }
        break;
    }
    
  }
}

//------------------------------------------------------------------
void writeScreen()
{
  screenTimer.updateTimer();
  if (screenTimer.timerDone())
  {
    //if we are updating the screen during a macro and the page isnt the macro active page
    if (internalMacroKeeper != 0 && activePage != MACROACTIVE && activePage != PID)
    {
      pageKeeper = activePage;  //record which page is active (to return to after macro)
      writeObject(FORMMESSAGE, MACROACTIVE, 0);    //change the page to the active macro page
      activePage = MACROACTIVE;
    }
    //figure out which page is displayed and
    //display only what you  need to
    switch (activePage)
    {
      case BATTERYMENU:
      
        writeObject(ANGULARMETER, 3, volts - 170);
        writeObject(LEDDIGITS, 8, volts);
        //amps-74
        writeObject(LEDDIGITS, 74, amps);
        //watts - 75
        writeObject(LEDDIGITS, 75, watts);
        writeObject(GUAGE, 0 , amps);
        break;
      case IROUT:
        writeObject(LEDDIGITS, 17, IRFrontLong);
        writeObject(LEDDIGITS, 24, IRFrontR);
        writeObject(LEDDIGITS, 25, IRFrontL);
        writeObject(LEDDIGITS, 15, IRRightLong);
        writeObject(LEDDIGITS, 16, IRLeftLong);
        writeObject(LEDDIGITS, 20, IRRightMid);
        writeObject(LEDDIGITS, 19, IRLeftMid);
        writeObject(LEDDIGITS, 21, IRBackL);
        writeObject(LEDDIGITS, 22, IRBackR);
        writeObject(LEDDIGITS, 18, IRBackLong);
        writeObject(LEDDIGITS, 23, IRBackMid);
        break;
      case AUTONOMOUSMENU:
        break;
      case TURNMENU:
        //led digits
        writeObject(LEDDIGITS, 0x01, abs(gyroAngle));
        break;
      case ACTUATORMENU:
        if (stateControl == 2)
          writeObject(USERLED, 0x03, 1);
        else
          writeObject(USERLED, 0x03, 0);
        //angular meter
        writeObject(ANGULARMETER , 0x00, bucketAngle);
        //led digits for actuator panel
        writeObject(LEDDIGITS , 0x09, bucketAngle);
        break;
      case CONSTANTSEND:
        writeObject(USERLED, 0, variableChosen);
        writeObject(LEDDIGITS , 68 , variableRequest);
        writeObject(USERLED, 1, valueChosen);
        writeObject(USERLED, 3, valueDispNeg);
        writeObject(LEDDIGITS , 69 , valueDispInt);
        writeObject(LEDDIGITS , 7 , valueDispExp);
        writeObject(USERLED, 4, negative);
        writeObject (LEDDIGITS, 2, workingRegDisp);
        writeObject(LEDDIGITS , 4 , tenToThe);
        writeObject(STRINGS , 1 , variableRequest);    //update the screen to the number requested.

        break;
      case IRFOLLOW:
        writeObject(LEDDIGITS , 64, L);
        writeObject(LEDDIGITS , 65, B);
        writeObject(LEDDIGITS , 66, R);
        writeObject(LEDDIGITS , 67, F);
        break;
      case MACROACTIVE:
        if (internalMacroKeeper == 0 && handy > 5)
        {
          writeObject(FORMMESSAGE, pageKeeper, 0);
          activePage = pageKeeper;
        }
        else
        {
          writeObject(LEDDIGITS , 76, L);  //Left IR
          writeObject(LEDDIGITS , 77, R);  //Right IR
          writeObject(LEDDIGITS , 78, F);  //Front IR
          writeObject(LEDDIGITS , 79, B);  //Back IR
          writeObject(LEDDIGITS , 80, abs(gyroAngle));  //gyroAngle
          writeObject(LEDDIGITS , 81, abs(commandBack.motorL));  //Left Motor
          writeObject(LEDDIGITS , 82, abs(commandBack.motorR));  //Right Motor
          writeObject(LEDDIGITS , 83, commandBack.macro_complete);  //complete
          writeObject(LEDDIGITS , 84, commandBack.macro_command);  //command
          writeObject(LEDDIGITS , 85, calcOutputHeading(robotHeading));  //Right Motor
          //NOT ON THIS PAGE  BUT I WANNA UPDATE THEM IN MACRO MODE SO IT UPDATES IN BACKGROUND
          writeObject(SCOPE, 0, commandBack.motorL);  //toComm.leftMotorSpeed);
          writeObject(SCOPE, 0, commandBack.motorR);  //toComm.rightMotorSpeed);
          writeObject(SCOPE, 1, commandBack.PIDerror);
        writeObject(SCOPE, 2, commandBack.PIDintegral/100);
        writeObject(SCOPE, 3, commandBack.PIDderivative*10);
        
          if (internalMacroKeeper == 0) handy += 1;
          else handy = 0;
        }
        break;
      case PID:
        //      if (commandBack.PIDerror < 0) writeObject(LED , 0x06, 1);
        //      else                       writeObject(LED , 0x06, 0);
        //      writeObject(LEDDIGITS , 86, abs(commandBack.PIDerror));  //error
        //      if (commandBack.PIDintegral < 0) writeObject(LED , 0x07, 1);
        //      else                           writeObject(LED , 0x07, 0);
        //      writeObject(LEDDIGITS , 87, abs(commandBack.PIDintegral));  //integral
        //      if (commandBack.PIDderivative < 0) writeObject(LED , 0x08, 1);
        //      else                             writeObject(LED , 0x08, 0);
        //      writeObject(LEDDIGITS , 88, abs(commandBack.PIDderivative));  //derivative
        //      if (commandBack.PIDoutput < 0)  writeObject(LED , 0x09, 1);
        //      else                          writeObject(LED , 0x09, 0);
        //      writeObject(LEDDIGITS , 89, abs(commandBack.PIDoutput));  //output
//        if (commandBack.PIDnumber != lastWrite);
//        {
//          writeObject(STRINGS , 0, commandBack.PIDnumber);  //name the PID
//          lastWrite = commandBack.PIDnumber;
//        }
        writeObject(SCOPE, 0, commandBack.motorL);  //toComm.leftMotorSpeed);
        writeObject(SCOPE, 0, commandBack.motorR);  //toComm.rightMotorSpeed);
        writeObject(SCOPE, 1, commandBack.PIDerror);
        writeObject(SCOPE, 2, commandBack.PIDintegral/10);
        writeObject(SCOPE, 3, commandBack.PIDderivative*10);
//         writeObject(SCOPE, 5, commandBack.PIDoutput);
       
       
        //   int t;
        //  while (1)
        //  {
        //    t=millis();
        //    writeObject(SCOPE,0, (sin((t/50)*(3.14159/16)))*100);
        //  }
        //delay(10);
        break;
      case MAINMENU:
        break;
      default:
        break;
    }
    screenTimer.resetTimer();
  }
}


inline int calcOutputHeading(int in)
{
  if (outputHeading <= 360 && outputHeading >= 0)
  {
  }
  //else condition the output
  else
  {
    while (outputHeading < 0)
    {
      outputHeading += 360;
    }
    while (outputHeading > 360)
    {
      outputHeading -= 360;
    }
  }
  outputHeading = 360 - outputHeading;
  return outputHeading;
}
inline int calcAndConstrainHeading(int calcHeading)
{
  while (calcHeading < 0)
  {
    calcHeading += 360;
  }
  while (calcHeading > 360)
  {
  }
  calcHeading = 360 - calcHeading;
  int offset = angle_command - calcHeading;
  while (offset > 360)
  {
    offset -= 360;
  }
  while (offset < -360)
  {
    offset += 360;
  }
  if (abs(offset) > 180)
  {
    if (offset > 0)
    {
      offset = offset - 360;
    }
    else if (offset < 0)
    {
      offset = offset + 360;
    }
  }
  return offset;
}

void whileNotMacro(int state)
{
  //while the control box and robot disagree
  while (commMacro != state)
  {
    //wait and check for state change
    if (communicationBoardOut.receiveData())
    {
      //if good checksum, store updated data locally.
      commMacro = commandBack.macro_command;
    }
    delay(5);
    //increment counter
    sender += 1;
    //if we have checked 10 times without a change
    if (sender >= 100)
    {
      //send the data again
      controlET.sendData();
      delay(5);
      //and reset the counter
      sender = 0;
    }
  }
}
void whileNotComplete()
{
  int complete = 0;
  //while the control box and robot disagree
  while (complete != 1)
  {
    //wait and check for state change
    if (communicationBoardOut.receiveData())
    {
      //if good checksum, store updated data locally.
      complete = commandBack.macro_complete;
    }
    delay(5);
    //increment counter
    sender += 1;
    //if we have checked 10 times without a change
    if (sender >= 100)
    {
      //send the data again
      controlET.sendData();
      delay(5);
      //and reset the counter
      sender = 0;
    }
  }
}



