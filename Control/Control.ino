
//control.ino
//
//Version 19
//Created by Zac Kilburn with guidence from Igor Vinograd :)
//3/7/14


//Software Packages Include
#include <EasyTransfer.h>
#include <EasyTransferCRC.h>
#include <Timers.h>
#include "SoftwareSerial.h"
#include "digitalWriteFast.h"
#include "Adafruit_Thermal.h"
#include <avr/pgmspace.h>

//Internal File Includes
#include "Methods.h"
#include "Definitions.h"
#include "Variables.h"
#include "Structs.h"
#include "Printer.h"
#include "MacroControl.h"
#include "Screen.h"
#include "Comms.h"
#include "PinMap.h"
#include "Debug.h"



void setup()
{ 
  //allow screen and system to gain power
  //printerPrint();
    delay(2500);
  initializeCommunicaton();
  initializePins(); 
//  for (int c=0; c<16; c++){    
//    writeContrast(c);
//    delay(1000);
//  }
  
    writeObject(FORMMESSAGE, pageKeeper, 0);
  
}

void loop()
{
  controlsUpdate();
  macroUpdate();
  updateCommunication();
  updateScreen();
}


