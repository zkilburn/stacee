
//screen.h
inline static bool writeObject(byte object, byte index, unsigned int data);
inline int calcAndConstrainHeading(int calcHeading);
inline int calcOutputHeading(int in);
inline static bool readObject();
inline void writeScreen();
inline static bool readStream();
inline static bool writeContrast(int value);

//macroControl.h
inline void ensureReceived(bool complete);

