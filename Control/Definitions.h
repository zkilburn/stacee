
//analog joystick pins
#define JOYSTICK_1_X A10
#define JOYSTICK_1_Y A11
#define JOYSTICK_2_X A12
#define JOYSTICK_2_Y A13

//pot pins for scaling
#define POT_2 A3
#define POT_1 A2

//set the digital io pins
#define ACTUATOR_UP 24
#define ACTUATOR_DOWN 23
#define ESTOP 22

//autonmous button pins
#define MACRO_BUTTON 25
#define AUTO_LIGHT 30

//set the speeds
#define NEUTRAL 127
#define MIN 1
#define MAX 255

//define the states of the robot
#define MANUAL 0
#define AUTONOMOUS 1

//blinking led pin
#define LED 13


//PRIMARY MACRO STATES
#define TURN 1
#define DRIVE 2
#define ACTUATOR 3
#define DDRIVE 4
#define IR 5

//ADDITIONAL MACRO STATES
#define additionalFeatures 68
#define gyroClear 1
#define reInit 2
#define scootch 3
#define autonomousFeatures 69
#define autoMode0 13
#define autoMode1 14
#define autoMode2 15
#define autoMode3 16
#define autoMode4 17
#define GOLDENAUTOBUTTON 46
#define fullAuto 0
#define constants 70
#define unstickIRs 3

//SCREEN COMMAND OBJECTS DEFINED
#define FORMMESSAGE 0x0A
#define SLIDER 0x04
#define KNOB 0x01
#define BUTTON 0x06
#define LEDDIGITS 0x0F
#define USERLED 0x13
#define ANGULARMETER 0x07
#define METER 0x10
#define LED 0x0E
#define GUAGE 0x0B
#define STRINGS 0x11
#define SCOPE 0x19
#define KEYBOARD 0x0D
#define CONTRAST 0x04

//SCREEN FORMS DEFINED AND LABELED FOR SMART UPDATE
#define DEBUGMENU 6
#define BATTERYMENU 1
#define MACROMENU 3
#define AUTONOMOUSMENU 2
#define DRIVEMENU 8
#define TURNMENU 4
#define GYROMENU 9
#define ACTUATORMENU 7
#define IROUT 10
#define MAINMENU 11
#define SYSTEMMONITOR 13
#define MONITORMANUAL 14
#define IRDEBUG 15
#define CALIBRATIONS1 16
#define IRSELECTION 17
#define CONSTANTSEND 18
#define IRFOLLOW 19
#define IRCALCSIZE 20
#define IRSMARTS 21
#define MACROACTIVE 22
#define PID 23
#define COMMALERT 3

//KEYPAD DEFINITIONS
#define BACK 98
#define ENTER 101

//Definitions for LED digits
#define RLF 36
#define ALF 37
#define RLR 38
#define ALR 39
#define RLL 40
#define ALL 41
#define RLB 42
#define ALB 43
#define RMR 44
#define AMR 45
#define RML 46
#define AML 47
#define RMB 48
#define AMB 49
#define RSBR 50
#define ASBR 51
#define RSBL 52
#define ASBL 53
#define RMFR 54
#define AMFR 55
#define RMFL 56
#define AMFL 57
//variable sender
#define COMMITTOSEND 75
#define WIPEREGS 76
//IR macro
#define FOLLOWRIGHT 73
#define FOLLOWLEFT 74
#define PERPENDICULAR 12
#define BACKANDDUMP 11
 #define DUMP 250
 #define MINE 145
 #define INITIR 15
 //start location buttons
 #define L1 17
 #define L2 19
 #define L3 28
 #define L4 29
 #define L5 34 
 #define L6 35
 #define R1 36
 #define R2 37
 #define R3 38
 #define R4 39
 #define R5 40
 #define R6 41


//Active macro screen
#define CANCELMACRO 77
//PID
#define CHANGEPID 79

//definitions for sliders
#define gyroLow 4
#define gyroHigh 5
#define motorLow 6
#define motorHigh 7

