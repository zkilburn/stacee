
inline void killMacro()
{
  stateControl = 1;
  internalMacroKeeper = 0;
  internalSubKeeper = 0;
  toComm.macro_command = 0;
  toComm.macro_sub_command = 0;
  toComm.macro_stop = 1;
  controlET.sendData();
  ensureReceived(true);
  commTimer.resetTimer();
}
inline void macroUpdate()
{
  //---------------------------------------------------------------------
  //FROM THE ROBOT -- MACRO COMPLETE
  if (internalMacroKeeper == 0)
  {
    toComm.macro_stop = 0;
  }
  //robot says done and we dont
  if (macro_complete == 1 && internalMacroKeeper != 0)
  {
    //When robot done --set state 1
    stateControl = 1;

    //internal systems reset
    internalMacroKeeper = 0;
    internalSubKeeper = 0;
    //tell the robot we are done as well
    toComm.macro_command = 0;
    toComm.macro_sub_command = 0;
    toComm.macro_stop = 0;
    controlET.sendData();
    ensureReceived(true);
    //RESET THE TRACKING VARIABLE
    macro_complete = 0;
    toComm.macro_stop = 0;
    commTimer.resetTimer();
  }
  //---------------------------------------------------------------------
  //FROM THE SCREEN -- MACRO PRESENT
  if (macro_command_screen != 0 && internalMacroKeeper == 0)
  {    
    pageKeeper = activePage;  //record which page is active (to return to after macro)
    writeObject(FORMMESSAGE, 5, 0);    //change the page to the macro confirm  

    while(!readObject())    
    {
      delay(5);
    }
    writeObject(FORMMESSAGE, pageKeeper, 0);
    //    if(receivedPack[2]==42)  //if its a button and its a yes process it
    //    {
    //Change control board state to macro state
    //let it know which macro
    internalMacroKeeper = macro_command_screen;
    internalSubKeeper = macro_sub_command;
    //change the state of control box to macro
    stateControl = 2;

    //Tell the comm board about the screens command
    toComm.macro_command = macro_command_screen;
    toComm.macro_sub_command = macro_sub_command;
    toComm.macro_stop = 0;
    controlET.sendData();
    ensureReceived(false);

    //erase the data from the screen
    macro_command_screen = 0;
    macro_sub_command = 0;
    toComm.macro_stop = 0;

    //    } 
    //    else
    //    {      
    //      macro_command_screen = 0;
    //      macro_sub_command = 0;
    //      toComm.macro_stop = 0;       
    //    }
    //change the page to the macro confirm  
  }
  //---------------------------------------------------------------------
  //BUTTON CONTROL -- STOP MACRO DURING EXECUTION
  if (digitalReadFast(MACRO_BUTTON) == HIGH)
  {
    killMacro();
  }
}


//complete==true if you are stopping macro or confirming end  
//complete==false iff you are starting macro
inline void ensureReceived(bool complete)
{
  //give it a second to try to recieve the first time reset the resend counter
  delay(5);
  sender = 0;
  //check and store for any valid packet
  if (communicationBoardOut.receiveData())
  {
    //if good checksum, store data locally.
    commMacro = commandBack.macro_command;
    macro_complete = commandBack.macro_complete;
  }
  if (complete == false)
  {
    if(internalMacroKeeper==0)
    {
      //while the robot has not gotten the command
      while (commMacro != internalMacroKeeper )
      {
        //update the information from the robot
        if (communicationBoardOut.receiveData())
        {
          //if good checksum, store data locally.
          commMacro = commandBack.macro_command;
        }

        //count how many times we have checked
        sender += 1;
        delay(5);
        //if we checked 5 times with out a change, resend data
        if (sender >= 50)
        {
          controlET.sendData();
          delay(15);
          //reset the timer
          sender = 0;
        }
      }
    }
    else 
    {
      //while the robot has not gotten the command
      while (commMacro != internalMacroKeeper )
      {
        //update the information from the robot
        if (communicationBoardOut.receiveData())
        {
          //if good checksum, store data locally.
          commMacro = commandBack.macro_command;
        }

        //count how many times we have checked
        sender += 1;
        delay(5);
        //if we checked 5 times with out a change, resend data
        if (sender >= 50)
        {
          controlET.sendData();
          delay(15);
          //reset the timer
          sender = 0;
        }
      }
    }
  }
  else
  {
    if (macro_complete==1)
    {
      //while the robot has not gotten word back from us
      while (macro_complete != 0)
      {
        //update the information from the robot
        if (communicationBoardOut.receiveData())
        {
          //if good checksum, store data locally.
          macro_complete = commandBack.macro_complete;
        }

        //count how many times we have checked
        sender += 1;
        delay(5);
        //if we checked 5 times with out a change, resend data
        if (sender >= 50)
        {
          controlET.sendData();
          delay(15);
          //reset the timer
          sender = 0;
        }
      }
    }
    else
    {
       //while the robot has not gotten the command
      while (commMacro != internalMacroKeeper )
      {
        //update the information from the robot
        if (communicationBoardOut.receiveData())
        {
          //if good checksum, store data locally.
          commMacro = commandBack.macro_command;
        }

        //count how many times we have checked
        sender += 1;
        delay(5);
        //if we checked 5 times with out a change, resend data
        if (sender >= 5)
        {
          controlET.sendData();
          delay(5);
          //reset the timer
          sender = 0;
        }
      }
    }

  }
}





