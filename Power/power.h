int amp[SIZE];
double totalAmps = 0;
unsigned long timeintervals = 0;
unsigned long totalVolts = 0;
int currentAmps;
int lastAmps = 0;
int lastAmps1 = 0;
int lastAmps2 = 0;
int currentVolts;
int lastVolts = 0;
long previousMillis = 0;
int index;
Timers LED(1000);
Timers readingT(20);
#define volts A2 //actual pin values need filled in
#define amps A0


void power()
{
  readingT.updateTimer();
  if (readingT.timerDone())
  {        
    currentVolts = map(analogRead(volts), 0, 1024, 0, 300); //the actual values need added when we know what they are
    totalAmps = totalAmps + ((currentAmps + lastAmps) / 2);
    totalVolts = totalVolts + ((currentVolts + lastVolts) / 2);
    
    amp[index]= map(analogRead(amps), 0, 1024, 0, 832);
    currentAmps = mean(amp,SIZE);  //the actual values need added when we know what they are
    
    
    lastVolts = currentVolts;
    timeintervals++;
    
    index+=1;
    if(index>=SIZE)
    {
     index=0; 
    }
  }

}
void updateLED()
{
  LED.updateTimer();
  static int state = HIGH;
  if (LED.timerDone())
  {
    state = !state;
    digitalWrite(13, state);

  }

}
