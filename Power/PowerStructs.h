Timers sender(250);

int averageVolts;
EasyTransfer powerBoardOut;
EasyTransfer powerBoardIn;
#define LED 13
struct CommandData
{
  boolean refresh;
};

struct PowerData
{
  long amphours;
  long watthours;
  long averageVolts;
};

CommandData request;
PowerData data;

void initializeCommunicaton()
{
  pinMode(LED, OUTPUT);
  Serial.begin(57600);
  powerBoardOut.begin(details(data), &Serial);
  powerBoardIn.begin(details(request), &Serial);
  for(int i=0;i<SIZE;i++)
  {
    amp[i]=map(analogRead(amps), 0, 1024, 0, 832);
  }
}



void calculatePower()
{
  double totalTime = timeintervals * 20;
  int hours = totalTime / 100 / 60 / 60;
  // = totalAmps * hours;
  //int averageAmps = totalAmps / timeintervals;
  data.amphours =currentAmps;
  data.averageVolts = totalVolts / timeintervals;
  long averageWatt = totalVolts * timeintervals;
  data.watthours = averageWatt * hours;

  //  data.amphours=analogRead(A0);
  //  data.averageVolts=analogRead(A1);
  //  data.watthours=analogRead(A2);
}

void updateCommunication()

{
  updateLED();
  sender.updateTimer();
  if (sender.timerDone())
  {
    calculatePower();
    powerBoardOut.sendData();
  }
}

