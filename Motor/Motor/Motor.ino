//motor.ino
//created by Igor Vinograd

//Motor control board
#include <EasyTransfer.h>
#include <Servo.h>
#include <digitalWriteFast.h>

#include "Structs.h"
#include "Comms.h"
#include "Servos.h"
#include "Pins.h"

void setup()
{
  initializeCommunication();
  initializeServos();
}

void loop()
{
  updateComms();
  updateServos();
  updateLed();
}
