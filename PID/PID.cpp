#include <PID.h>

//Library constructor
PID::PID(int target, float kp, float ki, float kd)
{	
    _target=target;
    _kp=kp;
	_ki=ki;
	_kd=kd;	
	_past=millis();
    _prevError=0;		
}
//External method
void PID::setPropotionality(float kp, float ki, float kd)
{
	_kp=kp;
	_ki=ki;
	_kd=kd;
}
void PID::clearIntegral()
{
     _integral=0;
}
void PID::verboseCalc()
{
     Serial.println();
    Serial.println();
    Serial.print("Derivative: ");
    Serial.print(_derivative);
    Serial.println();
    Serial.print("Integral: ");
    Serial.print(_integral);
    Serial.println();
    Serial.print("Error: ");
    Serial.print(_error);
    Serial.println();  
    Serial.print("Output: ");
    Serial.print(_output);
    Serial.println();
    Serial.println();
     
     }
//External method
int PID::updateOutput(int sample)
{
	//update dt in seconds
	_now=millis();
	_dt=(_now-_past)*1000;
	_past=_now;
	
	//update error and dError
	
	_error=_target-sample;	
	_dError=_error-_prevError;
	_derivative= _dError/_dt;
	_integral+= _error*_dt;
	_output=(_error*_kp)+(_derivative*_kd)+(_integral*_ki);
    _prevError=_error;
    return _output;
}

