/******************************************************************
*  	The PID library Header file
*				System allows for easy creation of a PID controlled
*				Updatable system
*		Brought to you by:
*              Zac Kilburn
******************************************************************/
#ifndef PID_h
#define PID_h
#include "Arduino.h"


class PID
{	
public:
	PID(int target, float kp, float ki, float kd);
/******************************************************************
This is where you will declare all of the methods
that will be able to be called by other segments of code externally
******************************************************************/
 int updateOutput(int sample);
 void setPropotionality(float kp, float ki, float kd);
 void clearIntegral();
 void verboseCalc();

private:
/******************************************************************
Declare all internal variables and methods that will only be called 
and used internally
******************************************************************/
float _kp, _ki, _kd;
float _dt;
float _output;
float _error,_prevError,_dError;
int _now, _past;
float _derivative, _integral;
int _target;

};
#endif
