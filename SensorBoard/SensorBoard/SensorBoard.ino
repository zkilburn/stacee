#include <EasyTransferCRC.h>
#include <Average.h>

const int pingPin = 7;
#define SIZE 10

int d0[SIZE];
int d1[SIZE];
int d2[SIZE];
int d3[SIZE];
int d4[SIZE];
int d5[SIZE];

int stddev0, avg0;
int stddev1, avg1;
int stddev2, avg2;
int stddev3, avg3;
int stddev4, avg4;
int stddev5, avg5;

int lastValue0 = 0;
int lastValue1 = 0;
int lastValue2 = 0;
int lastValue3 = 0;
int lastValue4 = 0;
int lastValue5 = 0;

int index = 0;

// establish variables for duration of the ping,
  // and the distance result in inches and centimeters:
  long duration, inches, cm;

EasyTransferCRC commBoard;

struct fromSensortoComm
{
  int data0;
  int data1;
  int data2;
  int data3;
  int data4;
  int data5;
};
static fromSensortoComm fromSensor;
int validRead(int signal,  int average);

void setup() {
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);
  
  // put your setup code here, to run once:
  Serial.begin(38400);
  commBoard.begin(details(fromSensor), &Serial);
  for (int i = 0; i < SIZE; i++)
  {
    d0[i] = analogRead(A0);
    d1[i] = analogRead(A1);
    d2[i] = analogRead(A2);
    d3[i] = analogRead(A3);
    d4[i] = analogRead(A4);
    d5[i] = readPing(pingPin);
    delay(5);
  }
  
}

void loop() {

  
  d0[index] = validRead(analogRead(A0), avg0);
  d1[index] = validRead(analogRead(A1), avg1);
  d2[index] = validRead(analogRead(A2), avg2);
  d3[index] = validRead(analogRead(A3), avg3);
  d4[index] = validRead(analogRead(A4), avg4);
  d5[index] = validRead(readPing(pingPin),avg5);
  

//  stddev0 = stddev(d0, SIZE);
//  stddev1 = stddev(d1, SIZE);
//  stddev2 = stddev(d2, SIZE);
//  stddev3 = stddev(d3, SIZE);
//  stddev4 = stddev(d4, SIZE);

  avg0 = mean(d0, SIZE);
  avg1 = mean(d1, SIZE);
  avg2 = mean(d2, SIZE);
  avg3 = mean(d3, SIZE);
  avg4 = mean(d4, SIZE);
  avg5 = mean(d5, SIZE);


   

  
  delay(5);
 
 
  fromSensor.data0 = avg0;
  fromSensor.data1 = avg1;
  fromSensor.data2 = avg2;
  fromSensor.data3 = avg3;
  fromSensor.data4 = avg4;
  fromSensor.data5 = avg5;

  commBoard.sendData();
  delay(15);

  // advance to the next position in the array:
  index += 1;

  // if we're at the end of the array...
  if (index >= SIZE)
    // ...wrap around to the beginning:
    index = 0;

}
int validRead(int signal, int average)
{
  int signalDifference = signal - average;
  return average + (signalDifference *0.98);
}


int readPing(int pin)
{
  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  //temporary ping sensor
  pinMode(pin, OUTPUT);
  digitalWrite(pin, LOW);
  delayMicroseconds(2);
  digitalWrite(pin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pin, LOW);

  // The same pin is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(pin, INPUT);
  duration = pulseIn(pin, HIGH);

  // convert the time into a distance   
  return microsecondsToCentimeters(duration);

}
long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}
